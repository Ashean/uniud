
/*
 * Modulo JosephusStatic:
 *
 * Programma per risolvere il problema di Giuseppe Flavio
 * (metodi statici))
 *
 * Ultimo aggiornamento: 29/04/2021
 */


public class JosephusStatic {


  public static int JosephusStatic( int n ) {
  
RoundTable_3 rt = new RoundTable_3( n );
    
    while ( rt.numberOfKnightsIn() > 1 ) {
      
      rt.serveNeighbour();
      rt.passJug();
    }
    return rt.knightWithJugIn();
  }

  
  // Eventuale programma di avvio
  
  public static void main( String args[] ) {
  
    int n = Integer.parseInt( args[0] );
    
    for ( int k=1; k<=n; k=k+1 ) {
      System.out.println( JosephusStatic(k) );
    }
  }


}  // class JosephusStatic
