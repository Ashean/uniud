
/*
 * Classe RoundTable_3:
 *
 * Modello alla base del problema di Giuseppe Flavio
 * (rivisto in termini di cavalieri attorno al tavolo)
 *
 * Gli oggetti creati incapsulano uno stato che evolve.
 *
 * Ultimo aggiornamento: 29/04/2021
 */


public class RoundTable_3 {


  // ----- Rappresentazione interna del modello: private!
  
  private int[] knights;            // array dei cavalieri (numerati)
  private int num;                  // numero dei cavalieri a tavola
  private int jug;                  // posizione cavaliere con brocca
  
  // ----- Costruttore
  
  public RoundTable_3( int n ) {      // creazione di una tavola
                                    // con n cavalieri
    knights = new int[ n ];
    
    for ( int k=1; k<=n; k=k+1 ) {
    
      knights[k-1] = k;
    }
    num = n;
    jug = 0;
  }

  
  // ----- Metodi del protocollo: acquisizione di informazioni sulla configurazione
  
  public int numberOfKnightsIn() {  // numero di cavalieri a tavola
  
    return num;
  }
  

  public int knightWithJugIn() {    // cavaliere con la brocca di sidro
  
    return knights[jug];
  }
  
  
  // ----- Metodi del protocollo: modifica della configurazione (mossa)
  
  public void serveNeighbour() {
  
    int next = ( jug + 1 ) % knights.length;
    
    knights[next] = knights[jug];   // il cavaliere servito
    jug = next;                     // abbandona la tavola
    num = num - 1;
  }
  
  public void passJug() {
  
    int last = ( jug + num ) % knights.length;
    int next = ( jug + 1   ) % knights.length;

    knights[last] = knights[jug];   // il cavaliere che cede la brocca
    jug = next;                     // passa in coda
  }


}  // class RoundTable_3
