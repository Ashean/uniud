
/*
 * Asserzioni e invarianti:
 *
 * Analisi di semplici programmi iterativi
 *
 * Ultimo aggiornamento: 23/05/21
 */


public class Invariants {


  // Analisi dei programmi iterativi tramite asserzioni e invarianti
  
  
  // Quadrato come somma di numeri dispari
  // (vedi definizione della procedura "unknown" in Scheme)
  
  public static int sqr( int n ) {  //  Pre:  n >= 0
    
    int x = 0;
    int y = 0;
    int z = 1;
    
    while ( x < n ) {  //  Inv:  y = x*x,  z = 2*x + 1,  x <= n
                       //  Term: n - x
      x = x + 1;
      y = y + z;
      z = z + 2;
    }
    return y;  //  Post:  y = n*n
  }
  
  
  // Cubo attraverso somme
  
  public static int cube( int n ) {  //  Pre:  n >= 0
  
    int x = 0;
    int y = 0;
    int u = 1;
    int v = 6;
    
    while ( x < n ) {  //  Inv:  0 <= x <= n,  y = x^3,  u = 3x^2 + 3x + 1,  v = 6x + 6
                       //  Term: n - x
      x = x + 1;
      y = y + u;
      u = u + v;
      v = v + 6;
    }
    return y;  //  Post:  y = n^3
  }
  
  
  // Moltiplicazione del "contadino russo"
  
  public static int mul( int a, int b ) {  //  Pre:  a >= 0,  b >= 0
    
    int x = a;
    int y = b;
    int z = 0;
    
    while ( y > 0 ) {     //  Inv:  x*y + z = a*b,  y >= 0
                          //  Term: y
      if ( y % 2 > 0 ) {
      
        z = z + x;
      }
      x = 2 * x;
      y = y / 2;
    }
    return z;  //  Post:  z = a*b
  }
  
  
  // Minimo comune multiplo
  
  public static int lcm( int m, int n ) {  // Pre:  m, n > 0
  
    int x = m;
    int y = n;
    
    while ( x != y ) {  // Inv:  0 < x, y <= mcm(m,n),  x mod m = y mod n = 0
                        // Term: 2mn - x - y
      if ( x < y ) {
        x = x + m;
      } else {
        y = y + n;
      }
    }
    return x;  // Post:  x = mcm(m,n)
  }
  
  
  // Fattorizzazione in fattori primi
  
  public static int[] factorization( int n ) {  // Pre:  n >= 2
  
    int[] fattori = new int[ n+1 ];
    
    for ( int i=0; i<=n; i=i+1 ) {
      fattori[i] = 0;
    }
    int x = n;
    int p = 2;
    
    while ( x > 1 ) {      // Inv:  1 <= x <= n,
                           //       n = x * Prod (k: [2,n]) k^fattori[k],
                           //       x non ha fattori < p,  2 <= p <= n
                           // Term: x + n - p
      if ( x % p == 0 ) {
        fattori[p] = fattori[p] + 1;
        x = x / p;
      } else {
        p = p + 1;
    }}
    return fattori;  // Post: n = Prod (k: [2,n]) k^fattori[k]
  }
  
  
  // Massimo comun divisore esteso
  
  public static int[] gcd( int a, int b ) {  //  Pre:  a > 0,  b > 0
    
    int x = a;
    int i = 1;
    int j = 0;
    
    int y = b;
    int k = 0;
    int l = 1;
    
    while ( x != y ) {  //  Inv:  MCD(x,y) = MCD(a,b),  x = i*a + j*b,  y = k*a + l*b
                        //  Term: x + y
      if ( x < y ) {
      
        y = y - x;
        k = k - i;
        l = l - j;
      
      } else {
      
        x = x - y;
        i = i - k;
        j = j - l;
      }
    }
    return new int[] { x, i, j };  //  Post:  x = MCD(a,b) = i*a + j*b
  }
  
  
  // Ricerca binaria in un array ordinato
  
  public static int pos( int x, int[] v ) {  //  Pre:  v ordinato,  esiste j t.c. v[j] = x
  
    int l = 0;
    int r = v.length - 1;
    int m;
    
    while ( l < r ) {   //  Inv:  l <= r,  esiste j in [l,r] t.c. v[j] = x
                        //  Term: r - l
      m = (l + r) / 2;
      
      if ( v[m] < x ) {
        l = m + 1;
      } else {
        r = m;
      }
    }
    return l;  //  v[l] = x
  }
  
  
  // Valore di un numerale rappresentato in notazione binaria
  
  public static int val( String b ) {  //  Pre:  b stringa di 0, 1
  
    int v = 0;
    int i = 0,  n = b.length();
    
    while ( i < n ) {  //  Inv:  v = Sum( bi * 2^(i-1-j) ) per j in [0,i-1],  i <= n
                       //  Term: n - i
                                         
      v = 2 * v + ( (b.charAt(i) == '0') ? 0 : 1 );
      i = i + 1;
    }
    return v;  //  Post:  v = Sum( bi * 2^(n-1-j) ) per j in [0,n-1]
  }
  
  
  // Ripartizione delle componenti di in un array
  
  public static int partition( int x, int[] v ) {  //  Pre:  esiste k t.c. v[k] <= x,  esiste k t.c. v[k] > x
  
    int i = 0;
    int j = v.length - 1;
    
    while ( i <= j ) {       //  Inv:  i <= j + 1,  ogni k < i . ( v[k] <= x ),  ogni k > j . ( x < v[k] )
                             //  Term: j - i + 1
      while ( v[i] <= x ) {
        i = i + 1;
      }
      while ( x < v[j] ) {
        j = j - 1;
      }
      if ( i < j ) {
        int t = v[i];
        v[i] = v[j];
        v[j] = t;
        i = i + 1;
        j = j - 1;
    }}
    return j;  //  Post:  ogni k <= j . ( v[k] <= x ),  ogni k > j . ( x < v[k] )
  }


}  // class Invariants