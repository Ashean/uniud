
/*
 * Tecniche di memoization e programmazione dinamica applicate
 * al problema della sottosequenza comune piu' lunga
 *
 * Ultimo aggiornamento: 29/04/2021
 */


public class LCS {


/* Lunghezza della sottosequenza comune piu' lunga (LLCS)

  (define llcs     ; valore: intero
    (lambda (u v)  ; u, v: stringhe
      (cond ((or (string=? u "") (string=? v ""))
             0)
            ((char=? (string-ref u 0) (string-ref v 0))
             (+ 1 (llcs (substring u 1) (substring v 1))))
            (else
             (max (llcs (substring u 1) v) (llcs u (substring v 1))))
            )))
*/
  
// Traduzione in Java:
  
  public static int llcs( String u, String v ) {
  
    if ( u.equals("") || v.equals("") ) {
      return  0;
    } else if ( u.charAt(0) == v.charAt(0) ) {
      return  1 + llcs( u.substring(1), v.substring(1) );
    } else {
      return  Math.max( llcs(u.substring(1),v), llcs(u,v.substring(1)) );
    }
  }
  
  
// Versione che applica la tecnica di memoization:
  
  public static int llcsMem( String u, String v ) {
  
    int m = u.length();
    int n = v.length();
    
    int[][] mem = new int[ m+1 ][ n+1 ];
    
    for ( int i=0; i<=m; i=i+1 ) {
      for ( int j=0; j<=n; j=j+1 ) {
        mem[i][j] = UNKNOWN;
    }}
    return llcsRec( u, v, mem );
  }
  
  private static int llcsRec( String u, String v, int[][] mem ) {
  
    int i = u.length();
    int j = v.length();
    
    if ( mem[i][j] == UNKNOWN ) {
    
      if ( (i == 0) || (j == 0) ) {
        mem[i][j] = 0;
      } else if ( u.charAt(0) == v.charAt(0) ) {
        mem[i][j] = 1 + llcsRec( u.substring(1), v.substring(1), mem );
      } else {
        mem[i][j] = Math.max( llcsRec(u.substring(1),v,mem),
                              llcsRec(u,v.substring(1),mem) );
    }}
    return mem[i][j];
  }
  
  private static final int UNKNOWN = -1;  // 0 e' un valore ammissibile
  
  
/* Sottosequenza comune piu' lunga (LCS)

  (define lcs      ; valore: stringa
    (lambda (u v)  ; u, v: stringhe
      (cond ((or (string=? u "") (string=? v ""))
             "")
            ((char=? (string-ref u 0) (string-ref v 0))
             (string-append (substring u 0 1)
                            (lcs (substring u 1) (substring v 1))))
            (else
             (longer (lcs (substring u 1) v) (lcs u (substring v 1))))
            )))
*/
  
// Traduzione in Java:
  
  public static String lcs( String u, String v ) {
  
    if ( u.equals("") || v.equals("") ) {
      return  "";
    } else if ( u.charAt(0) == v.charAt(0) ) {
      return  u.charAt(0) + lcs( u.substring(1), v.substring(1) );
    } else {
      return  longer( lcs(u.substring(1),v), lcs(u,v.substring(1)) );
    }
  }
  
  private static String longer( String u, String v ) {
  
    int m = u.length();
    int n = v.length();
    
    if ( m < n ) {
      return v;
    } else if ( m > n ) {
      return u;
    } else if ( Math.random() < 0.5 ) {  // scelta causale se m = n
      return v;
    } else {
      return u;
    }
  }
  
  
// Versione che applica la tecnica di memoization:
  
  public static String lcsMem( String u, String v ) {
  
    int m = u.length();
    int n = v.length();
    
    String[][] mem = new String[ m+1 ][ n+1 ];
    
    for ( int i=0; i<=m; i=i+1 ) {
      for ( int j=0; j<=n; j=j+1 ) {
        mem[i][j] = null;
    }}
    return lcsRec( u, v, mem );
  }
  
  private static String lcsRec( String u, String v, String[][] mem ) {
  
    int i = u.length();
    int j = v.length();
    
    if ( mem[i][j] == null ) {
    
      if ( (i == 0) || (j == 0) ) {
        mem[i][j] = "";
      } else if ( u.charAt(0) == v.charAt(0) ) {
        mem[i][j] = u.charAt(0) + lcsRec( u.substring(1), v.substring(1), mem );
      } else {
        mem[i][j] = longer( lcsRec(u.substring(1),v,mem),
                            lcsRec(u,v.substring(1),mem) );
    }}
    return mem[i][j];
  }
  
  
// Versione che applica la tecnica di programmazione dinamica:
// i, j rappresentano indici di suffissi di lunghezza m-i, n-j
  
  public static String lcsDp( String u, String v ) {
  
    int m = u.length();
    int n = v.length();
    
    // Sezione I: LLCS
    
    int[][] mem = new int[ m+1 ][ n+1 ];
    
    for ( int i=0; i<=m; i=i+1 ) {
      for ( int j=0; j<=n; j=j+1 ) {
      
        if ( (i == 0) || (j == 0) ) {
          mem[i][j] = 0;
        } else if ( u.charAt(m-i) == v.charAt(n-j) ) {
          mem[i][j] = 1 + mem[i-1][j-1];
        } else {
          mem[i][j] = Math.max( mem[i-1][j], mem[i][j-1] );
        }
    }}                // mem[0][0] = LLCS(u,v)
    
    // Sezione II: LCS
    
    String lcs = "";  // Percorso attraverso la matrice mem
    int i = m;
    int j = n;
    while ( mem[i][j] > 0 ) {
    
      if ( u.charAt(m-i) == v.charAt(n-j) ) {
        lcs = lcs + u.charAt(m-i);
        i = i - 1;
        j = j - 1;
      } else if ( mem[i-1][j] < mem[i][j-1] ) {
        j = j - 1;
      } else if ( mem[i-1][j] > mem[i][j-1] ) {
        i = i - 1;
      } else if ( Math.random() < 0.5 ) {
        j = j - 1;
      } else {
        i = i - 1;
    }}
    return lcs;       // lcs = LCS(u,v)
  }
  
  
// Soluzione alternativa che applica la tecnica di programmazione dinamica:
  
  public static String lcsDp2( String u, String v ) {
  
    int m = u.length();
    int n = v.length();
    
    String[] mem = new String[ n+1 ];
    
    for ( int j=n; j>=0; j=j-1 ) {
      mem[j] = "";
    }
    for ( int i=m-1; i>=0; i=i-1 ) {
    
      String w = "";
      for ( int j=n-1; j>=0; j=j-1 ) {
        if ( u.charAt(i) == v.charAt(j) ) {
          String t = u.charAt(i) + w;
          w = mem[j];
          mem[j] = t;
        } else {
          w = mem[j];
          mem[j] = longer( mem[j], mem[j+1] );
        }
    }}
    return mem[0];
  }
  
  
  public static void main( String[] args ) {
  
    String u = args[0];
    String v = args[1];
    
    System.out.println( "Mem: lcs(" + u + "," + v + ") = " + lcsMem(u,v) + " : " + llcsMem(u,v) );
    System.out.println( "Dp:  lcs(" + u + "," + v + ") = " + lcsDp(u,v)  );
    System.out.println( "Dp2: lcs(" + u + "," + v + ") = " + lcsDp2(u,v) );
    System.out.println( "Rec: lcs(" + u + "," + v + ") = " + lcs(u,v)    + " : " + llcs(u,v)    );
  }
  
}  // class LCS


/* Esempi:

javac LCS.java

java LCS arto atrio
java LCS salva saliva
java LCS solido alito
java LCS stelvio trivio
java LCS capzioso cupido
java LCS cortigiana triglia
java LCS semplificazione affioramento
java LCS strabiliante abbigliamento
java LCS approvigionamento parametrico
*/
