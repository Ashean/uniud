
public class TestIntSLists {


  /* Lista che rappresenta un intervallo di interi consecutivi:
  
  (define range        ; val: lista di interi
    (lambda (inf sup)  ; inf, sup: interi
      (if (> inf sup)
          null
          (cons inf (range (+ inf 1) sup))
          )))
  */

  public static IntSList range( int inf, int sup ) {
  
    if ( inf > sup ) {
      // return new IntSList();
      return IntSList.NULL_INTLIST;
    } else {
      // return new IntSList( inf, range(inf+1,sup) );
      return range( inf+1, sup ).cons( inf );
    }
  }
  
  
  /* Inserimento ordinato in una lista ordinata di interi:

  (define ins           ; val: lista ordinata di interi
    (lambda (x s)       ; x: intero, s: lista ordinata di interi
      (cond ((null? s)
             (cons x null))
            ((< x (car s))
             (cons x s))
            (else
             (cons (car s) (ins x (cdr s))))
            )))
  */
  
  public static IntSList ins( int x, IntSList s ) {  // s ordinata
  
    if ( s.isNull() ) {
      return ( new IntSList() ).cons(x);
    } else if ( x < s.car() ) {
      return s.cons(x);
    } else {
      return ins(x,s.cdr()).cons(s.car());
    }
  }


}  // class TestIntSLists


/* ----- Esempi di valutazioni di espressioni:

  TestIntSLists.range( 1, 0 );
  TestIntSLists.range( 1, 1 );
  TestIntSLists.range( 1, 10 );
  ( TestIntSLists.range(1,10) ).reverse();
  ( TestIntSLists.range(1,10) ).append( TestIntSLists.range(1,10).reverse() );

*/
