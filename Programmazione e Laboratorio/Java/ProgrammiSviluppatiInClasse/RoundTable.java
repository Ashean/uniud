
/*
 * Classe RoundTable:
 *
 * Modello alla base del problema di Giuseppe Flavio
 * (rivisto in termini di cavalieri attorno al tavolo)
 *
 * Gli oggetti creati sono "immutabili".
 *
 * Ultimo aggiornamento: 25/03/2021
 *
 *
 * Protocollo:
 *
 *   RoundTable ini = new RoundTable( n );
 *
 *   RoundTable cfg ...
 *
 *     cfg.numberOfKnights() :  int
 *     cfg.kinghtWithJug()   :  int  (etichette)
 *
 *     cfg.serveNeighbour()  :  RoundTable
 *     cfg.passJug()         :  RoundTable
 */


public class RoundTable {


  // ----- Rappresentazione interna del modello: private!
  //       oggetti immutabili: final
  
  private final int num;                   // numero di cavalieri a tavola
  private final int jug;                   // etichetta del cavaliere con la brocca
  private final IntSList others;           // lista degli altri cavalieri (numerati)
  
  
  // ----- Costruttore pubblico
  
  public RoundTable( int n ) {             // creazione di una tavola
                                           // con n cavalieri
    num = n;
    jug = 1;
    others = range( 2, n );
  }
  
  
  // ----- Costruttore non pubblico di supporto
  
  private RoundTable( int n, int j, IntSList o ) {
  
    num = n;
    jug = j;
    others = o;
  }
  
  
  // ----- Metodi del protocollo: acquisizione di informazioni sulla configurazione
  
  public int numberOfKnights() {           // numero di cavalieri a tavola
  
    return num;
  }
  

  public int knightWithJug() {             // etichetta del cavaliere
                                           // con la brocca di sidro
    return jug;
  }
  
  
  // ----- Metodi del protocollo: generazione di configurazioni successive
  
  public RoundTable serveNeighbour() {     // serve il commensale vicino a sinistra:
                                           // il commensale servito lascia la tavola
    if ( num > 1 ) {
      return new RoundTable( num-1, jug, others.cdr() );
    } else {
      return this;                         // meno di due commensali
    }
  }
  
  
  public RoundTable passJug() {            // passa la brocca al commensale vicino
                                           // (a sinistra)
    if ( num > 1 ) {
      IntSList last = IntSList.NULL_INTLIST.cons( jug );
      IntSList list = ( others.cdr() ).append( last );
      return new RoundTable( num, others.car(), list );
    } else {
      return this;                         // meno di due commensali
    }
  }
  
  
  // ----- Procedura interna di supporto (private!)
  
  private static IntSList range( int inf, int sup ) {
  
    if ( inf > sup ) {
      return IntSList.NULL_INTLIST;
    } else {
      return range( inf+1, sup ).cons( inf );
    }
  }


}  // class RoundTable
