import java.util.*;
/**
 * Aggiungi qui una descrizione della classe Examms
 * 
 * @author (Ashean Abeysinghe) 
 * @version (0.0)
 */
public class Es
{

    /*1. Memoization (15 Luglio 2014)
    La procedura llcs3 determina la lunghezza della sottosequenza comune più lunga (LLCS) di tre stringhe:

    (define llcs3
    (lambda (t u v)
    (cond ((or (string=? t "") (string=? u "") (string=? v ""))
    0)
    ((char=? (string-ref t 0) (string-ref u 0) (string-ref v 0))
    (+ 1 (llcs3 (substring t 1) (substring u 1) (substring v 1))))
    (else
    (max (llcs3 (substring t 1) u v)
    (llcs3 t (substring u 1) v)
    (llcs3 t u (substring v 1))))
    )))s
     */

    public int llcs3 (String v, String u, String t){
        if( v.equals("") || u.equals("") || t.equals("")){
            return 0;}
        if ((v.charAt(0) == u.charAt(0)) && (v.charAt(0) == t.charAt(0))){
            return (1 + (llcs3 ((v.substring(1)),(u.substring(1)),(t.substring(1)))));
        }else{
            return Math.max(Math.max((llcs3((v.substring(1)),u ,t)), (llcs3(v,(u.substring(1)),t))), (llcs3(v, u,(t.substring(1)))) );
        }
    }

    /**
    public int llcs3DP(String v, String u, String t){
    int[][][] mem = new int[v.length()][u.length()][t.length()];
    for(int x = 0; x< mem.length; x++){
    for(int y = 0; y< mem.length[0]; y++){
    for(int z = 0; z< mem.length[0][0]; z++){
    if( v.equals("") || u.equals("") || t.equals("")){
    mem[i] = 0;
    }
    }
    }
    return mem.length;
    }
    }**/

    private static String longer( String u, String v, String t ) {

        int m = u.length();
        int n = v.length();
        int p = t.length();

        if ( m > p ) {
            if(m > n){
                return u;
            }  else{
                return v;
            }
        } else if (p > n){
            return t;
        }else{
            return v;
        }
    }

    public static String lcsMem( String u, String v, String t ) {
        int m = u.length();
        int n = v.length();
        int p = t.length();

        String[][][] mem = new String[ m+1 ][ n+1 ][ p+1 ];

        for ( int i=0; i<=m; i=i+1 ) {
            for ( int j=0; j<=n; j=j+1 ) {
                for(int k=0; k<=p; k=k+1){
                    mem[i][j][k] = null;
                }
            }
        }
        return  lcsRec( u, v, t, mem );
    }

    private static String lcsRec( String u, String v,String t, String[][][] mem ) {

        int i = u.length();
        int j = v.length();
        int z = t.length();

        if ( mem[i][j][z] == null ) {

            if ( (i == 0) || (j == 0) || (z==0)) {
                mem[i][j][z] = "";
            } else if (( u.charAt(0) == v.charAt(0)) && (v.charAt(0) == t.charAt(0))) {
                mem[i][j][z] = u.charAt(0) + lcsRec( u.substring(1), v.substring(1), t.substring(1), mem );
            } else {
                mem[i][j][z] = longer( lcsRec(u.substring(1),v,t,mem),
                    lcsRec(u,v.substring(1),t,mem),
                    lcsRec(u,v,t.substring(1),mem));
            }}
        return mem[i][j][z];
    }

    /**
    2. Programmazione in Java (15 Luglio 2014)
    Come è noto dall’algebra lineare, una matrice quadrata Q si dice simmetrica se coincide con la propria trasposta,
    ovvero se Qij = Qji per tutte le coppie di indici della matrice. Scrivi un metodo statico in Java per verificare se
    l’argomento è una matrice simmetrica — assumendo che tale argomento rappresenti una matrice quadrata.
    Per quanto possibile, struttura il codice in modo tale da ridurre al minimo il numero di confronti effettuati dal
    programma nei casi in cui la matrice sia effettivamente simmetrica.**/

    public static boolean simmetria (int[][] matr_a){
        //Non effettuo un controllo per verificare se le matrici siano quadrate o meno, però controllo se abbiano la stessa dimensione
        int i = matr_a.length;

        boolean sim = true;

        for(int r=0; r < i ; r++){
            for(int c = 0; c < r; c++){
                if(matr_a[r][c] == matr_a[c][r]){
                    sim = true;
                }else{
                    sim = false;
                    r=0;
                    i=0;
                }
            }
        }
        return sim;
    }

    /**
    3. Oggetti in Java (9 Settembre 2015)
    Il modello della scacchiera realizzato dalla classe Board per affrontare il rompicapo delle n regine deve essere integrato
    introducendo due nuovi metodi: isFreeRow(int), che dato un indice di riga i compreso fra 1 e la dimensione n della
    scacchiera, restituirà true se e solo se nella riga i non c’è alcuna regina; addQueen(String), che svolge la stessa
    funzione di addQueen(int,int) ma ricevendo come argomento la codifica della posizione tramite una stringa di due
    caratteri, una lettera per la colonna e una cifra per la riga secondo le convenzioni consuete. Inoltre, addQueen e
    removeQueen non devono modificare lo stato della scacchiera se l’operazione è inconsistente perché due regine
    verrebbero a trovarsi sulla stessa casella oppure perché nella posizione data non c’è una regina da rimuovere. Per
    esempio, il metodo statico listOfCompletions, definito sotto a destra, stamperà tutte le soluzioni del rompicapo, se
    ve ne sono, compatibili con una disposizione iniziale di regine che non si minacciano reciprocamente.
    In base a quanto specificato sopra, modifica opportunamente la classe Board.**/

    //L'esercizio è stato completato direttamente nella esercitazione del 21/04/2021 - Problema 12
    // isFreeRow è stato completato, addQueens(Strings) pure

    /*
     * Dato un albero di Huffman, il metodo statico shortestCodeLength determina la lunghezza del più corto fra i codici
    di Huffman associati ai caratteri che compaiono in un documento di testo. Più specificamente, la visita dell’albero,
    finalizzata alla determinazione di tale lunghezza, è realizzata attraverso uno schema iterativo.
    Completa la definizione del metodo shortestCodeLength riportata nel riquadro.
     */
    public static int shortestCodeLength( Node root ) {
        int sc = root.weight();
        Stack<Node> stack = new Stack<Node>();
        Stack<Integer> depth = new Stack<Integer>();
        stack.push( root );
        depth.push( 0 );
        do {
            Node n = stack.pop();
            int d = depth.pop();
            if ( n.isLeaf() ) {
                sc = Math.min( sc, d );
            } else if ( d+1 < sc) {
                stack.push(n.left());
                stack.push(n.right());
                depth.push(d+1);
                depth.push(d+1);         
            }
        } while ( !stack.empty() && !depth.empty());
        return sc;
    }

    /**
     * 5. Programmazione in Java (5 Settembre 2016)
    Dato un array di numeri (double) con almeno due elementi, il metodo statico closestPair ne restituisce una
    coppia la cui differenza in valore assoluto è minima. La coppia è rappresentata da un array ordinato di due elementi.
    Esempio:
    closestPair( new double[] {0.3, 0.1, 0.6, 0.8, 0.5, 1.1} ) → {0.5, 0.6}
    Definisci in Java il metodo statico closestPair
     */
    
    public static double[] closestPair(double [] arr){
        
        
        double[] orderedArray = tidyArray(arr, 0);
        double [] pair = new double[]{orderedArray[0], orderedArray[1]};
        double diff = Math.abs(orderedArray[0] - orderedArray[1]);
        for(int i = 1; i< orderedArray.length -1 ; i++){
            if(diff > Math.abs(orderedArray[i] - orderedArray[i+1])){
                pair[0] = orderedArray[i];
                pair[1] = orderedArray[i+1];
                diff = Math.abs(orderedArray[i] - orderedArray[i+1]);
            }
        }
        
        return pair;
        //Partiamo con un ordinamento dell' array
    }
    
    public static double[] tidyArray(double [] arr, int i ){
        if( i < arr.length-1)
            if(arr[i] > arr[i+1]){
                double scambio = arr[i];
                arr[i] = arr[i+1];
                arr[i+1] = scambio;
                tidyArray(arr,0);
            }else{
                tidyArray(arr, i+1);
            }
        
        return arr;
    }

}
