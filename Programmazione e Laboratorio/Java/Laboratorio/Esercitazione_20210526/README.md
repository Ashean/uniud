﻿

**Laboratorio di Programmazione**

**a.a. 2020-21**

**Progetto “Codifica di Huffman”**

26 Maggio 2021

**1. Coda con Priorità**

Definisci una classe NodeQueue che possa sostituire PriorityQueue<Node> nel programma Huffman offrendo

le stesse funzionalità della classe predefinita quando gli oggetti inseriti sono di tipo Node. Il protocollo deve quindi

prevedere il costruttore e i metodi così specificati:

public NodeQueue()

public int size()

*costruttore: creazione della coda di nodi vuota*

*restituisce il numero di elementi contenuti nella coda*

*restituisce l’elemento con “peso minore” (senza rimuoverlo dalla coda)*

*restituisce e rimuove dalla coda l’elemento con “peso minore”*

*aggiunge un nuovo elemento* n *alla coda*

public Node peek()

public Node poll()

public void add( Node n )

Realizza la rappresentazione interna utilizzando strumenti base di Java, in particolare gli *array*, senza ricorrere

all’importazione delle classi rese disponibili dal package di supporto java.util .

**2. Stack (pila)**

Analogamente al punto precedente, definisci una classe NodeStack per sostituire Stack<Node> nei metodi che

rielaborano iterativamente gli schemi ricorsivi utilizzati nei programmi di compressione e decompressione, garantendo

le stesse funzionalità della classe predefinita. Il protocollo deve prevedere il costruttore e i metodi così specificati:

public NodeStack()

*costruttore: creazione di uno stack di nodi vuoto*

*verifica se lo stack è vuoto*

public boolean empty()

public Node peek()

*restituisce l’elemento in cima allo stack (senza rimuoverlo dallo stack)*

*restituisce l’elemento in cima allo stack e lo rimuove dallo stack*

*aggiunge un nuovo elemento* n *in cima allo stack*

public Node pop()

public void push( Node n )

Anche in questo caso, realizza la rappresentazione interna utilizzando strumenti base di Java, in particolare gli *array*,

senza ricorrere all’importazione delle classi rese disponibili dal package di supporto java.util .

**3. Verifica**

Verifica infine la correttezza delle soluzioni utilizzando (senza modificarla!) la versione del programma Huffman

collegata a questa parte del progetto nella sezione delle pagine del corso dedicata al Laboratorio, versione che fa

riferimento alle classi NodeQueue e NodeStack che avrai realizzato in accordo con i requisiti forniti sopra.

