
/**
 * Aggiungi qui una descrizione della classe NodeStack
 * 
 * @author (il tuo nome) 
 * @version (un numero di versione o una data)
 */
public class NodeStack
{
    // variabili d'istanza - sostituisci l'esempio che segue con il tuo
    private Node[] stack; 
    private int size;
    public NodeStack(){
        stack = new Node[0];
        size = 0; 
    }// costruttore: creazione di uno stack di nodi vuoto
    public boolean empty(){
        return (size == 0);
    }// verifica se lo stack è vuoto
    public Node peek(){
        if(this.empty()){
            return null;
        }else {
            return stack[size-1];
        }
    }// restituisce l’elemento in cima allo stack (senza rimuoverlo dallo stack)
    public Node pop(){
        if(this.empty()){
            return null;
        }else{
            Node top = peek(); 
            size--;
            Node[] cutted_stack = new Node[size];
            for(int i = 0; i<size; i++){
                cutted_stack[i] = this.stack[i];
            }
            this.stack = cutted_stack;
            return top;
        }
    }//restituisce l’elemento in cima allo stack e lo rimuove dallo stack
    public void push( Node n ){

        Node[] cutted_stack = new Node[size+1];
        for(int i = 0; i<size; i++){
            cutted_stack[i] = this.stack[i];
        }
        size++;
        cutted_stack[size-1] = n;
        this.stack = cutted_stack;
    } //aggiunge un nuovo elemento n in cima allo stack

}
