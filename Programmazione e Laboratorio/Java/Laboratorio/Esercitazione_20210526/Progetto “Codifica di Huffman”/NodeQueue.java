
public class NodeQueue {

    // variabili d'istanza - sostituisci l'esempio che segue con il tuo
    private int size;
    private Node[] elements;
    //private int[] priority;

    /**
     * Costruttore degli oggetti di classe  NodeQueue
     */
    public NodeQueue() {

        // inizializza le variabili d'istanza
        size = 0;
        elements = new Node[128];
        //priority = new int[127];

    }

    public int size() {

        return size;
    }

    private Node result;
    public Node peek() {
        result = null;
        
        result = elements[0];
        for(int i=0;i<size;i++) {
            
            if( result.weight() > elements[i].weight() ) {
                result = elements[i];
            }
        }
        
        return result;
    }

    public Node poll() {
        Node[] buffer = new Node[128];

        result = peek();
        int count = 0;
        //for(int i=0;i<size;i++) {
        int i = 0;
        while(elements[i] != result) {
            buffer[i] = elements[i];
            i++;
            count = i;
        }
        for(int j=count; j<elements.length-1; j++) {
            buffer[j] = elements[j+1];   
        }

        elements = buffer;
        size = size - 1;
        return result;
    }


    public void add(Node n) {  
        
        elements[size] = n;
        size++;
    }

    public void sorted() {
        for (int i = 0; i < size; i++)   
        {  
            for (int j = i + 1; j < size; j++)   
            { 
                Node tmp = null;  
                if (elements[i].weight() > elements[j].weight())   
                {  
                    tmp = elements[i];  
                    elements[i] = elements[j];  
                    elements[j] = tmp;  
                }  
            }}
    }
    

} //class NodeQueue
