
/**
 * Aggiungi qui una descrizione della classe StringSList
 * 
 * @author (Ashean Abeysinghe) 
 * @version (0.0)
 * 
 * Descrizione – Parte I
Definisci in Java una classe StringSList per rappresentare liste di stringhe nello stile di Scheme. Analogamente alla
classe introdotta a lezione per le liste di interi, il protocollo deve prevedere i seguenti costruttori e metodi:

 public StringSList() // null
 
 public StringSList( String e, StringSList sl ) // cons
 
 public boolean isNull() // null?
 public String car() // car 
 
 public StringSList cdr() // cdr
 
 public StringSList cons( String e ) // cons (modalità alternativa)
 
 public int length() // length
 
 public String listRef( int k ) // list-ref 
 
 public boolean equals( StringSList sl ) // equal?
 
 public StringSList append( StringSList sl ) // append
 
 public StringSList reverse() // reverse
 
 public String toString() // visualizzazione testuale
 */
public class StringSList
{
     // ----- Costante lista vuota (condivisa)
    public static final StringSList NULL_STRINGLIST = new StringSList();
  
  // ----- Rappresentazione interna di una lista: private!
  
  private final boolean empty;             // oggetti immutabili:
  private final String first;                 // variabili di istanza "final"
  private final StringSList rest;
    
  
      /**
     * Costruttore degli oggetti di classe  StringSList
     */
    public StringSList() //null creazione di una lista vuota 
    {
        empty = true;
        first = "";
        rest = null;
    }

    public StringSList( String e, StringSList sl ){
        empty = false;                            //creazione di una lista
        first = e;                                //Scheme :  cons
        rest = sl; 
    }
    
    public boolean isNull(){
        return empty;
    }
    
    public String car(){
        return first;
    }
    
    public StringSList cdr(){
        return rest;
    }
    
    public StringSList cons( String e ) {          // costruzione di nuove liste                                       // Scheme: cons
        return new StringSList( e, this );
    }
  
    public int length(){
        if(isNull()){
            return 0;
        }else{
            return (1 + cdr().length());
        }
    }
    
     public String listRef( int k ) {
        if(k==0){
            return car();
        }else{
            return (cdr().listRef(k-1));
        }
    }
    
    public boolean equals(StringSList il){
        if(isNull()||il.isNull()){
            return (isNull() && il.isNull());
        }else if (car()==il.car()){
            return cdr().equals(il.cdr());
        }else{
            return false;
        }
    
    }
    
    public StringSList append (StringSList il){
        if(isNull()){
            return il;
        }else{
            return (cdr().append(il)).cons(car());
        }
    }
    
    public StringSList reverse(){
        return reverseRec(new StringSList());
    }
    
    private StringSList reverseRec( StringSList re){
        if(isNull()){
        return re;
        }else {
        return cdr().reverseRec(re.cons(car()));
    
        }
    }
    
    public String toString(){
        if(isNull()){
            return "()";
        }else{
            String rep = "(" + car();
            StringSList r = cdr();
            while (!(r.isNull())){
                rep = rep + "," + r.car();
                r = r.cdr();
            }
            return (rep +")");
        }
    }
}
    
    
