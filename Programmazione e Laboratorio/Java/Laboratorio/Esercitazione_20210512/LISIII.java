
/**
 * Aggiungi qui una descrizione della classe LongestIncreasingSubsequence
 * 
 * @author (Ashean Abeysinghe) 
 * @version (0.0)
 */
public class LISIII
{
    // variabili d'istanza - sostituisci l'esempio che segue con il tuo
    public static int llis( int[] s ) { // s[i] > 0 per i in [0,n-1], dove n = s.length
        return llisRec( s, 0, 0 );
    }
    private static int llisRec( int[] s, int i, int t ) {
        if ( i == s.length ) { // i = n : coda di s vuota
            return 0;
        } else if ( s[i] <= t ) { // x = s[i] ≤ t : x non può essere scelto
            return llisRec( s, i+1, t );
        } else { // x > t : x può essere scelto o meno
            return Math.max( 1+llisRec(s,i+1,s[i]), llisRec(s,i+1,t) );
        }
    }
    
    //Necessitiamo di usare MAX_VALUE poiché dobbiamo assicurarci che nella
    //prima iterazione ricorsiva s[i] sia minore del valore (t).
    
    //Così poi nella chiamata successiva si troverà il numero successivamente
    //decrescente a t
    
    /*Lasciando infatti lldsRec(s,0,0) la funzione non ritorna i valori corretti
       per esempi come llds(new int[] {10, 8, 9, 5, 6, 7, 1, 2, 3, 4} )
       */
    public static int llds( int[] s ) { // s[i] > 0 per i in [0,n-1], dove n = s.length
        
        return lldsRec( s, 0, Integer.MAX_VALUE );

    }
    private static int lldsRec( int[] s, int i, int t ) {
        if ( i == s.length ) { // i = n : coda di s vuota
            return 0;
        } else if ( s[i] >= t ) { // x = s[i] >= t : x non può essere scelto
            return lldsRec( s, i+1, t );
        } else { // x > t : x può essere scelto o meno
            return Math.max( 1+lldsRec(s,i+1,s[i]), lldsRec(s,i+1,t) );
        }
    }
    
    //Funzione che trova il valore massimo all'interno di un array
    private static int getMaxValue(int[] numbers){
        int maxValue = numbers[0];
        for(int i=1;i < numbers.length;i++){
        if(numbers[i] > maxValue){
          maxValue = numbers[i];
        }
      }
      return maxValue;
     }

     
     public static int lldsDP( int[] s ) {
  
    int n = s.length;
    
    int[][] mem = new int[ n+1 ][ n+1 ];
    
    int t = Integer.MAX_VALUE;
    
    // Matrice: valori delle ricorsioni di llisRec
    // relativi a diversi valori degli argomenti
    
    for ( int j=0; j<=n; j=j+1 ) {
    
      // --------------------------------------------------
      //  Inserisci qui i comandi per registrare i valori
      //  corrispondenti ai casi base della ricorsione
      // --------------------------------------------------
      
      // Gli ultimi elementi dei casi base della ricorsione 
      // sono tutti 0 nella ultima colonna con indice n
      mem[j][n]= 0 ;
    }
    
    /*Teoricamente, guardando la figura in liss_bottom_up.pdf,
       per il valore j si indica il contatore delle righe, mentre
       per il valore i si indica il contatore delle colonne
       pertanto nel seguente codice si segue la logica dell'immagine*/
         
    
      for ( int i=n-1; i>=0; i=i-1 ) {
           for ( int j=0; j<=n; j=j+1 ) {
        // ------------------------------------------------
        //  Inserisci qui le strutture di controllo
        //  appropriate e i comandi per registrare
        //  i valori corrispondenti ai casi ricorsivi
        // ------------------------------------------------
         if(j == n){
            t=Integer.MAX_VALUE;
        }else{
            t=s[j];
        }
        if((j<i) || (j==n)){  /*Salta gli elementi superflui per il calcolo*/
            if(s[i]<t){    //t = s[j] se 0 ≤ j < n
                mem[j][i]= Math.max(mem[j][i+1],1+mem[i][i+1]);
            }else{
                mem[j][i]= mem[j][i+1];
            }
        }
            
       
        }
        
    }
    // ----------------------------------------------------
    //  Inserisci di seguito l'elemento della matrice
    //  il cui valore corrisponde a llds(s) :
    
    return  mem[n][0]/* elemento appropriato della matrice */;
    
    // ----------------------------------------------------
  }


  
  // Longest Decreasing Subsequence (LDS):
  // Programmazione dinamica bottom-up
  
  public static int[] ldsDP( int[] s ) {
  
    int n = s.length;
    
    int[][] mem = new int[ n+1 ][ n+1 ];
    
    // 1. Matrice: valori delle ricorsioni di llisRec
    //    calcolati esattamente come per llisDP
    
    // ------------------------------------------------
    //  Replica qui il codice del corpo di llisDP
    //  che registra nella matrice i valori
    //  corrispondenti alle ricorsioni di llisRec
    //
    for ( int j=0; j<=n; j=j+1 ) {
    
      // --------------------------------------------------
      //  Inserisci qui i comandi per registrare i valori
      //  corrispondenti ai casi base della ricorsione
      // --------------------------------------------------
      mem[n][j] =  0;
    }
    
    for ( int i=n-1; i>=0; i=i-1 ) {
      for ( int j=0; j<=n; j=j+1 ) {
      
        // ------------------------------------------------
        //  Inserisci qui le strutture di controllo
        //  appropriate e i comandi per registrare
        //  i valori corrispondenti ai casi ricorsivi
        // ------------------------------------------------
        int t = Integer.MAX_VALUE;
        if (j != n) {
          t = s[j];
        }
        
        if( j == n || j < i ) {
          if ( s[i] < t ) {
            mem[i][j] = Math.max( 1 + mem[i+1][i], mem[i+1][j] );
          } else {
            mem[i][j] = mem[i+1][j];
          }
        }
      }
    }
    // ------------------------------------------------
    
  
    // 2. Cammino attraverso la matrice per ricostruire
    //    un esempio di Longest Increasing Subsequence
    
    // ----------------------------------------------------
    //  Inserisci di seguito l'elemento della matrice
    //  il cui valore corrisponde a llis(s) :

    int m =  mem[0][n]; //elemento appropriato della matrice;
    
    // ----------------------------------------------------
    
    int[] r = new int[ m ];  // per rappresentare una possibile LIS
    
    // ----------------------------------------------------
    //  Introduci e inizializza qui gli indici utili
    //  per seguire un cammino attraverso la matrice e
    //  per assegnare gli elementi della sottosequenza r
    int j = n;
    int i = 0;
    int k = 0;
    // ----------------------------------------------------

    while ( mem[i][j] > 0 ) {
    
      int t = ( j == n ) ? 0 : s[j];
      
      // --------------------------------------------------
      //  Inserisci qui strutture di controllo e comandi
      //  per scegliere e seguire un percorso appropriato
      //  attraverso la matrice in modo da ricostruire in
      //  r una possibile LIS relativa alla sequenza s
      
      int x = mem[i+1][j];
      int y = mem[i][j];
      int z = mem[i+1][i];
      
      if ( y == x ){
        i = i + 1;
      } else if( y == 1 + z ){
        j = i;
        r[k] = s[j];
        k = k + 1;
        i = i + 1;
      }
      // --------------------------------------------------
    }
    
    return r;                // = LDS relativa alla sequenza s
}
  } // class LISIII


