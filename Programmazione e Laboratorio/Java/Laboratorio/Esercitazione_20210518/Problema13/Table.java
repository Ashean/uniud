import puzzleboard.PuzzleBoard;
/**
 * Aggiungi qui una descrizione della classe Table
 * 
 * @author (il tuo nome) 
 * @version (un numero di versione o una data)
 */
public class Table
{
    // variabili d'istanza - sostituisci l'esempio che segue con il tuo
    private int n;
    public int[][] matrix;
    private PuzzleBoard gui;
    //private int[] val_array;

    /**
     * Costruttore degli oggetti di classe  Table
     */

    public Table(int m)
    {
        n = m;
        matrix = new int[n][n];
        int count = 0;
        
        //val_array= new int[n*n];
        //val[0] = 0;   //il primo elemento nella matrice ordinata è la lacuna
        for(int r = 0; r < n ; r++){
            for(int c = 0 ; c < n ; c++){
                matrix[r][c] = count;
                count++;
            }

        }

    }

    public boolean isOrdered(){
        boolean ordered = true;
        int count = 0;
        for(int r=0; r < n; r++){
            for(int c=0; c < n; c++){
                ordered = (matrix[r][c] == count) && ordered;
                count++;
            }
        }       
        return ordered; 
    }

    public boolean canMoveLeft(int x, int y){
        boolean left = false;
        if( y != 0){
            left = (matrix[x][y-1]==0);
        }
        return left;
    }

    public boolean canMoveRigth(int x, int y){
        boolean rigth = false;
        if( y != (n-1)){
            rigth = (matrix[x][y+1]==0);
        }
        return rigth;
    }

    public boolean canMoveUpper(int x, int y){
        boolean up = false;
        if( x != 0){
            up = (matrix[x-1][y]==0);
        }
        return up;
    }

    public boolean canMoveDown(int x, int y){
        boolean down = false;
        if( x != (n-1)){
            down = (matrix[x+1][y]==0);
        }
        return down;
    }

    public boolean canMove(int x, int y){
        return canMoveLeft(x,y) || canMoveRigth(x,y) || canMoveUpper(x,y) || canMoveDown(x,y) ;
    }

    public String toString(){
        String config = "";
        for(int r = 0 ; r<n ; r++){
            for(int c = 0 ; c < n ; c++){
                config = config +" - "+ matrix[r][c];
            }
            config =config + "\\n";
        }
        return config;

    }

    public void Move(int x,int y){
        if(canMove(x,y)){

            if (canMoveLeft(x,y)){
                matrix[x][y-1] =+ matrix[x][y]; 
                matrix[x][y] = 0;
            }else if (canMoveRigth(x,y)){
                matrix[x][y+1] =+ matrix[x][y]; 
                matrix[x][y] = 0;
            }else if(canMoveUpper(x,y)){
                matrix[x-1][y] =+ matrix[x][y]; 
                matrix[x][y] = 0;
            }else if(canMoveDown(x,y)){
                matrix[x+1][y] =+ matrix[x][y]; 
                matrix[x][y] = 0;

            }
        }
        
        /* In questa funzione poteva funzionare anche un switch and case, ma data la natura della classe si sarebbe dovuto cambiare la logica dei boolean.
         * Conoscendo comunque l'inefficienza degli if annidati ho ritenuto comunque adatto, dato il ridotto numero degli if, tenere il codice così
         */
    }

}
