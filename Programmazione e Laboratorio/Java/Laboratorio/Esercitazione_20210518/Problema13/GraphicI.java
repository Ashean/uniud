import puzzleboard.PuzzleBoard;
public class GraphicI {

    // variabili d'istanza - sostituisci l'esempio che segue con il tuo
    private Table game_board;
    private PuzzleBoard gui;
    /**
     * Costruttore degli oggetti di classe  GUI
     */
    public GraphicI(int n) {
        game_board = new Table(n);
        gui = new PuzzleBoard(n);

        for(int i=1;i<=n;i++) {
            for(int j=1;j<=n;j++) {
                gui.setNumber( i,j, game_board.matrix[i-1][j-1] );

            }}

        /* 
        int f=0;
        while(f==0){
        int k = gui.get();
        int[] xy = research(k);

        if (game_board.canMove(xy[0],xy[1])){

                
        game_board.Move(xy[0], xy[1]);
        //gui.display(); 
        gui.clear( xy[0]+1, xy[1]+1);
        gui.display();
        }
        //gui.display();  
        //}*/
    }

    private int[] research(int k) {
        int len = game_board.matrix.length;
        int[] res = new int[2];
        for(int i=0;i<len;i++) {
            for(int j=0;j<len;j++) {
                if(k == game_board.matrix[i][j]) {
                    res[0] = i;
                    res[1] = j;
                }
            }}   
        return res;
    }

    public void Play() {
        while(true){
            int k = gui.get();
            int[] xy = research(k);

            if (game_board.canMove(xy[0],xy[1])){
                //int d =  game_board.matrix[xy[0]][xy[1]];
                if (game_board.canMoveLeft(xy[0],xy[1])){              
                    game_board.Move(xy[0],xy[1]);
                    gui.setNumber(xy[0]+1, xy[1], k);
                    gui.clear(xy[0]+1, xy[1]+1);
                }else if (game_board.canMoveRigth(xy[0],xy[1])){
                    game_board.Move(xy[0],xy[1]);
                    gui.setNumber(xy[0]+1, xy[1]+2, k);
                    gui.clear(xy[0]+1, xy[1]+1);
                }else if(game_board.canMoveUpper(xy[0],xy[1])){
                    game_board.Move(xy[0],xy[1]);
                    gui.setNumber(xy[0], xy[1]+1, k);
                    gui.clear(xy[0]+1, xy[1]+1);
                }else if(game_board.canMoveDown(xy[0],xy[1])){
                    game_board.Move(xy[0],xy[1]);
                    gui.setNumber(xy[0]+2, xy[1]+1,k);
                    gui.clear(xy[0]+1, xy[1]+1);

                }
                gui.display();
            }  //

        }
    }

}//class GUI
