
/*
 * Rompicapo delle "n regine"
 *
 * Ultimo aggiornamento: 24/04/2018
 *
 *
 * Realizzazione del dato astratto "configurazione della scacchiera"
 *
 * Protocollo della classe Board:
 *
 *   new Board( int n )           :  costruttore (scacchiera vuota)
 *
 *   size()                       :  int
 *
 *   queensOn()                   :  int
 *
 *   underAttack( int i, int j )  :  boolean
 *
 *   addQueen( int i, int j )     :  Board
 *
 *   arrangement()                :  String
 *
 *
 * Board b;
 *
 *   new Board(n)           costruttore della scacchiera n x n vuota;
 *   b.size()               dimensione n della scacchiera b;
 *   b.queensOn()           numero di regine collocate nella scacchiera b;
 *   b.underAttack(i,j)     la posizione <i,j> e' minacciata?
 *   b.addQueen(i,j)        scacchiera che si ottiene dalla configurazione b
 *                          aggiungendo una nuova regina in posizione <i,j>
 *                          (si assume che la posizione non sia minacciata);
 *   b.arrangement() :      descrizione "esterna" della configurazione
 *                          (convenzioni scacchistiche).
 */


import java.util.function.*;


public class Board {


  // Codifica secondo le convenzioni scacchistiche (massima dimensione: 15 x 15)
  
  private static final String ROWS = " 123456789ABCDEF";
  private static final String COLS = " abcdefghijklmno";
  
  
  // Realizzazione del dato astratto "Scacchiera": stato interno
  
  private final int size;                             // 1) dimensione scacchiera
  private final int queens;                           // 2) numero regine collocate
  
  //private final BiPredicate<Integer,Integer> attack;  // 3) predicato: posizione minacciata?
  
  private final IntSList underAttackCol;              //3a) posizione delle col minacciate
  private final IntSList underAttackRow;
  private final IntSList underAttackDiagSx;
  private final IntSList underAttackDiagDx;
  
  private final String config;                        // 4) disposizione delle regine:
                                                      //    rappresentazione testuale
  
  // Costruttori:
  
  public Board( int n ) {                             // scacchiera vuota
  
    size = n;                                         // scacchiera n x n
    queens = 0;                                       // nessuna regina
    
    //attack = ( u, v ) -> false;                       // nessuna posizione minacciata
    
    underAttackCol = new IntSList();
    underAttackRow = new IntSList();
    underAttackDiagSx = new IntSList();
    underAttackDiagDx = new IntSList();

    config = " ";
  }
  
  private Board( Board board, int i, int j) {        // <-- board.addQueen(i,j)
    
    size = board.size();                              // stessa dimensione di board
    queens = board.queensOn() + 1;                    // ma una nuova regina che...
    
    underAttackCol =  board.underAttackCol.cons(j);
    underAttackRow =  board.underAttackRow.cons(i);
    underAttackDiagSx = board.underAttackDiagSx.cons(i+j);
    underAttackDiagDx = board.underAttackDiagDx.cons(i-j);
    
    /*attack = ( u, v ) -> ( (u == i) ||                // minaccia la riga i
                           (v == j) ||                // minaccia la colonna j
                           (u-v == i-j) ||            // minaccia le diagonali i-j e i+j
                           (u+v == i+j) ||
                           board.underAttack(u,v)     // minaccia delle regine in board
                           );
    */
    config = board.arrangement() + COLS.charAt(j) + ROWS.charAt(i) + " ";
  }
  
  
  public int size() {                                 // dimensione della scacchiera
  
    return size;
  }
  
  
  public int queensOn() {                             // numero di regine collocate
  
    return queens;
  }
  
  public IntSList underAttackColGe(){
    return underAttackCol;
    }
    public IntSList underAttackRow(){
    return underAttackRow;
    }
    public IntSList underAttackDiagSx(){
    return underAttackDiagSx;
    }
    public IntSList underAttackDiagDx(){
    return underAttackDiagDx;
    }
  
  private boolean underAttackListCheck (IntSList list, int i, int j){
    boolean not_attack = true;
      for(int k = 0; k <= list.length() -1; k++){
        not_attack =not_attack && (list.listRef(k) != i+j);
        }
        return !not_attack; 
    }
  
  public boolean underAttack( int i, int j ) {        // posizione <i,j> minacciata?
      
      return (underAttackListCheck(underAttackCol, 0,j)   ||
              underAttackListCheck(underAttackRow, i,0)   ||
              underAttackListCheck(underAttackDiagSx,i,j) ||
              underAttackListCheck(underAttackDiagDx,i,-j));
      
    //return ( attack.test(i,j) );
  }
  
  
  public Board addQueen( int i, int j ) {             // nuova scacchiera
                                                      // con una regina anche in <i,j>
    return new Board( this, i, j );
  }
  
  
  public String arrangement() {                       // descrizione testuale
  
    return config;
  }
  
  
  public String toString() {                          // rappresentazione standard per Java
  
    return "[" + config + "]";
  }


}  // class Board
