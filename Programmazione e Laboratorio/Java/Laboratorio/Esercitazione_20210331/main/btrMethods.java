
/**
 * Questa invece è una classe di metodi, trai quali c'è anche btr_succ
 * 
 * @author (Ashean Abeysinghe) 
 * @version (0.0)
 */
public class btrMethods
{
    // variabili d'istanza - sostituisci l'esempio che segue con il tuo
    public String btr_succ(String in_btr)
    {
        
        int n = in_btr.length();
        char lsb =  in_btr.charAt(n - 1);
        
        if( n == 1){
            if(lsb == '+'){
            return "+-";
            }else{
            return "+";}
        }else{
                String pre = new String (in_btr.substring(0,n-1));
                if(lsb == '+'){
                    return btr_succ(pre)+"-";
                }else{
                    if(lsb == '-'){
                        return pre + ".";
                    }else{
                        return pre + "+";
                    }
                }
            
        }
    }

    
}
