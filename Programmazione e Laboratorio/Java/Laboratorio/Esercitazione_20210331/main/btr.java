
/**
 * Aggiungi qui una descrizione della classe btr
 * 
 * Ho creato una classe per l' oggetto btr_bilanciato, in modo tale che
 * btr_succ sia un metodo di ogni oggetto di tipo btr
 * 
 * @author (Ashean Abeysinghe) 
 * @version (30/03/2021)
 */
public class btr
{
    // variabili d'istanza - sostituisci l'esempio che segue con il tuo
    private String value;
    

    /**
     * Costruttore degli oggetti di classe  btr
     */
    public btr()
    {
        // inizializza le variabili d'istanza
        value = ".";
    }
    
    
    public btr(String input)
    {
        // inizializza le variabili d'istanza
        value = input;
    }

    /**
     * Un esempio di metodo - aggiungi i tuoi commenti
     * 
     * @param  y   un parametro d'esempio per un metodo
     * @return     la somma di x e y
     */
    public String btr_succ(btr this){
       btr x = this;
    return btr_succMeth(x); 
    }
    
    private String btr_succMeth(btr in_btr)
    {
        
        int n = in_btr.value.length();
        char lsb =  in_btr.value.charAt(n - 1);
        
        if( n == 1){
            if(lsb == '+'){
            return "+-";
            }else{
            return "+";}
        }else{
                btr pre = new btr(in_btr.value.substring(0,n-1));
                if(lsb == '+'){
                    return btr_succMeth(pre)+"-";
                }else{
                    if(lsb == '-'){
                        return pre + ".";
                    }else{
                        return pre + "+";
                    }
                }
            
        }
        
        
    }
}
