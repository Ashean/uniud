
/**
 * Aggiungi qui una descrizione della classe binaries
 * 
 * @author (Ashean Abeysinghe) 
 * @version (0.0)
 */
public class binaries
{
    // variabili d'istanza - sostituisci l'esempio che segue con il tuo
    private String bin ;

    /**
     * Costruttore degli oggetti di classe  binaries
     */
    public binaries()
    {
        // inizializza le variabili d'istanza
        bin = "0";
    }
     public binaries(String in)
    {
        // inizializza le variabili d'istanza
        bin = in;
    }
    
    private String bitComplement(String bit){
        if(bit.equals("0")){
            return "1";
        }else{
            return "0";
        }
    }
    /**
     * Un esempio di metodo - aggiungi i tuoi commenti
     * 
     * @param  y   un parametro d'esempio per un metodo
     * @return     la somma di x e y
     */
    public String  oneComplement()
    {
        String out_string = new String();
        for(int i = 0 ; i < bin.length(); i++){
            out_string=out_string + bitComplement(String.valueOf(bin.charAt(i))) ;
        }
        return out_string;
    }
}
