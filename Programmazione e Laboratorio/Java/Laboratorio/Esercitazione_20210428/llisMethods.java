
/**
 * Aggiungi qui una descrizione della classe llis
 * 
 * @author Ashean Abeysinghe 
 * @version 0.0
 */
public class llisMethods{

    private static final int UNKNOWN = -1;

    // variabili d'istanza - sostituisci l'esempio che segue con il tuo
    public static int llis( int[] s ) { // s[i] > 0 per i in [0,n-1], dove n = s.length
        return llisRec( s, 0, 0 );
    }

    private static int llisRec( int[] s, int i, int t ) {
        if ( i == s.length ) { // i = n : coda di s vuota
            return 0;
        } else if ( s[i] <= t ) { // x = s[i] ≤ t : x non può essere scelto
            return llisRec( s, i+1, t );
        } else { // x > t : x può essere scelto o meno
            return Math.max( 1+llisRec(s,i+1,s[i]), llisRec(s,i+1,t) );
        }
    }

    public static int llisMem( int[] s ) {
        int[][] mem = new int[s.length+1][s.length+1];
        int i = 0;
        int t = 0;
        for(int k=0 ; k<mem.length; k++){
            for(int j=0; j<mem[0].length;j++){
                mem[k][j]=UNKNOWN;
            }
        }
        return llisRecMem(s,i,t,mem);
    }

    private static int llisRecMem( int[] s, int i, int t, int[][] mem ) {
        if(mem[t][i] == UNKNOWN){
            if ( i == s.length ) { // i = n : coda di s vuota
                mem[t][i] = 0;
            } else if ( s[i] <= t ) { // x = s[i] ≤ t : x non può essere scelto
                mem[t][i] = llisRecMem( s, i+1,t , mem);
            } else { // x > t : x può essere scelto o meno
                mem[t][i] = Math.max( 1+llisRecMem(s,i+1,s[i],mem), llisRecMem(s,i+1,t,mem) );
            }
        }
        return (mem[t][i]);
    }

    /**
    public static int llisMem( int[] s ) {
    int[]mem = new int[s.length+1];
    int i = 0;
    int t = 0;
    mem[i] = s[i];
    return llisRecMem(s,i,t,mem);
    }
    private static int llisRecMem( int[] s, int i, int t, int[] mem ) {

    if ( i == s.length ) { // i = n : coda di s vuota
    mem[i] = 0;
    } else if ( mem[t] <= t ) { // x = s[i] ≤ t : x non può essere scelto
    mem[i] = llisRecMem( s, i+1,t , mem);
    } else { // x > t : x può essere scelto o meno
    mem[i] = Math.max( 1+llisRecMem(s,i+1,mem[t],mem), llisRecMem(s,i+1,t,mem) );
    }
    return (mem[t]);
    }
     **/

    public static int llisMemGen( int[] s ) {
        int[][] mem = new int[s.length+1][s.length+1];
        //int[] s_1 = new int [s.length]; //mem per tenere i valori molto grandi usando t come indice
        int i = 0;
        int t = s.length;
       
        for(int k=0 ; k<mem.length; k++){
            for(int j=0; j<mem[0].length;j++){
                mem[k][j]=UNKNOWN;
            }
        }
        return llisRecMemGen(s,i,t,mem);
    }

    private static int llisRecMemGen( int[] s, int i, int j, int[][] mem ) {
            if (mem[i][j] == UNKNOWN) {
            if (i == s.length) { 
                return 0;
            } else if (j==s.length) { //non ci sono elemento > di s[i]
                mem[i][j] = 1+llisRecMemGen(s, i + 1, i, mem);
            } else if (s[i] <= s[j]) {  //non abbiamo ancora iterato tutto l' array, dobbiamo controllare che ci siano degli elem >
                mem[i][j]= llisRecMemGen(s, i + 1, j, mem); //esiste un elem >, quindi lo salvi e procedi per il prossimo elem di s
            } else { 
                mem[i][j]= Math.max(1 + llisRecMemGen(s, i + 1, i, mem), llisRecMemGen(s, i+1, j, mem)); //valuto se procedere con il prossimo elem si s oppure
            }
        }
        return Math.max(mem[i][j], llisRecMemGen(s, i + 1, j, mem));

    }

    
    /** Versione sviluppata usando la programmazione dinamica **/
    
    private static int getMaxValue(int[] numbers){
        int maxValue = numbers[0];
        for(int i=1;i < numbers.length;i++){
            if(numbers[i] > maxValue){
                maxValue = numbers[i];
            }
        }
        return maxValue;
    }

    private static int getMinValue(int[] numbers){
        int minValue = numbers[0];
        for(int i=1;i<numbers.length;i++){
            if(numbers[i] < minValue){
                minValue = numbers[i];
            }
        }
        return minValue;
    }

    public static int test (){
        return 10;
    }

    public static int llisDP( int[] s ){
        int[] sPoints = new int[s.length];  //array dove salvo i punteggi delle varie subsequence
        int i = 0;
        int j = 0;                        //contatori base per 
        for(int k=0 ; k<=sPoints.length; k++){
            sPoints[k]=1;
        }
        for(; j<= s.length;j++){
            for(;i==j;){
                if(s[j]>=(s[i]+1)){
                    sPoints[j] = Math.max(i+1,j);
                    i++;
                }else{
                    i++;
                }
            }
            i=0;
        }

        return getMaxValue(sPoints);
    }

}
