

/**
 * Aggiungi qui una descrizione della classe Exam
 * 
 * @author (il tuo nome) 
 * @version (un numero di versione o una data)
 */
public class Exam
{
    // variabili d'istanza - sostituisci l'esempio che segue con il tuo
    public static int llis( double[] s ) {  // s[i] > 0  per  i in [0,n-1],  dove n = s.length

        return llisRec( s, 0, 0 );

    }


    private static int llisRec( double[] s, int i, double t ) {

        if ( i == s.length ) {  // i = n :  coda di s vuota

            return 0;

        } else if ( s[i] <= t ) {  // x = s[i] ≤ t :  x non può essere scelto

            return llisRec( s, i+1, t );

        } else {  // x > t :  x può essere scelto o meno

            return Math.max( 1+llisRec(s,i+1,s[i]), llisRec(s,i+1,t) );

        }

    }

    public static int llmds (double[] s){
        return llmdsRec(s,1,s[0]);
    }

    private static int llmdsRec(double[] s, int i, double t){
        if ( i == s.length-1 ) {  // i = n :  coda di s vuota

            return 0;

        } else if (s[i+1]>=(t*1/2) &&  s[i+1]<t) { //0.5 s[i] ≤ s[i+1] < s[i]

            return llmdsRec( s, i+1, t );

        } else {  // x > t :  x può essere scelto o meno

            return Math.max(1+ llmdsRec(s,i+1,s[i]), llmdsRec(s,i+1,t) );

        }
    }
    
    public static int llmds2( double[] s ) { 

	      return llmdsRec2( s, 1,s[0]);

	  }
	
	public static int llmdsRec2(double[] s,int i,double t) {
	      if ( i == s.length-1) {

	          return 0;

	      } else if (!(s[i]>= (t*1/2)&& s[i]<t)) {

	          return llmdsRec( s, i+1, t );

	      } else {

	          return Math.max( 1+llmdsRec(s,i+1,s[i]), llmdsRec(s,i+1,t) );

	      }
	      
	}
	
	
	public static int llmdsMem ( double[] s ) {  
		  	int len=s.length;
			 double max = 0;
			 for(int i=0; i<s.length; i++) {
				 if(s[i] >= max) {
					 max = s[i];
				 }
			 }
			 
			 int[][] mem = new int[len+1][len+1];
			 for(int x=0; x<=len; x++) {
				 for(int y=0; y<=len; y++) {
					 mem[x][y] = -1;
				 }
			 }
			 
		      return llmdsRecMem( s, 1, max, mem );

		  }
	  
	  private static int llmdsRecMem( double[] s, int i, double t, int[][] mem ) {
		  int len=s.length;
	          if(mem[len][len] == -1) {
	    	   if ( i == s.length-1 ) {  

	    		   mem[len][len] = 0;

	           } else if ( !(s[i+1] >= (t*1/2) &&  s[i+1]<t)) {  

	    	           mem[len][len] = llmdsRecMem( s, i+1, t, mem );

	      } else {  

	    	  mem[len][len] = Math.max( 1+llmdsRecMem(s,i+1,s[i], mem), llmdsRecMem(s,i+1,t, mem) );

	      }
	      }
		 return mem[len][len];

	  }
	  
	    public static String manhattanPath( int[][] manh ){
    	       String best_path="";
    	       
    	       return manhattanPathRec(manh,0,0, best_path);
	        
	    }
	    
	    private static String manhattanPathRec( int[][] manh, int i,int j, String best_path){
	       
	        if(i != manh.length-1 || i!= manh.length-1){
        	           if( cost(manh,i+1,j) <= cost(manh,i,j+1)){
        	           best_path += "D";
        	           return manhattanPathRec(manh, i +1, j, best_path);
        	           }else{
        	               best_path += "R";
        	           return manhattanPathRec(manh, i +1, j, best_path);
        	           }
	           }
	       
	       return best_path;
	       }
}

