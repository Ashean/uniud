
/**
 * Aggiungi qui una descrizione della classe compito_A
 * 
 * @author (il tuo nome) 
 * @version (un numero di versione o una data)
 */
public class compito_A
{
    /*2. Programmazione dinamica
    Considera la seguente procedura funzionale (metodo statico), basata su una ricorsione ad albero:
     */
    public static long rec( int x, int y, int z ) { // 1 <= x, y <= z
        if ( (x == 1) || (y == z) ) {
            return 1;
        } else {
            return rec( x-1, y, z ) + x * rec( x, y+1, z );
        }
    }

    public static long recDP( int x, int y, int z ) { // 1 <= x, y <= z
        long[][][] mem =new long[x+1][y+1][z+1];
        for(int r=0; r<x;r++){
            for(int c=0; c<y;c++){
                for(int k=0; k<z;k++){

                    if ( (r == 1) || (c == k) ) {
                        mem[r][c][k]= 1;
                    } else {
                        mem[r][c][k]= recDP(r-1, c ,k ) + r * recDP( r, c+1, k );
                    }
                }
            }
            }
            return mem[1][y][z];
    }
  /*    public static long fibonacciMem( int n ) {  // n >= 0
  
    long[] mem = new long[ n+1 ];
    
    for ( int i=0; i<=n; i=i+1 ) {
      mem[i] = UNKNOWN;
    }
    return fibonacciRec( n, mem );
  }
  
  private static long fibonacciRec( int n, long[] mem ) {
  
    if ( mem[n] == UNKNOWN ) {
    
      if ( n < 2 ) {
        mem[n] = 1;
      } else {
        mem[n] = fibonacciRec( n-2, mem ) + fibonacciRec( n-1, mem );
    }}
    return mem[n];
  }*/
    private int x;

    /**
     * Costruttore degli oggetti di classe  compito_A
     */
    public compito_A()
    {
        // inizializza le variabili d'istanza
        x = 0;
    }

    /**
     * Un esempio di metodo - aggiungi i tuoi commenti
     * 
     * @param  y   un parametro d'esempio per un metodo
     * @return     la somma di x e y
     */
    public int sampleMethod(int y)
    {
        // metti qui il tuo codice
        return x + y;
    }
}
