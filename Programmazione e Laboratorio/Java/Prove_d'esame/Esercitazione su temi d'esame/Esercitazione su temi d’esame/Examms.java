
/**
 * Aggiungi qui una descrizione della classe Examms
 * 
 * @author (Ashean Abeysinghe) 
 * @version (0.0)
 */
public class Examms
{

    /*1. Memoization (15 Luglio 2014)
    La procedura llcs3 determina la lunghezza della sottosequenza comune più lunga (LLCS) di tre stringhe:

    (define llcs3
    (lambda (t u v)
    (cond ((or (string=? t "") (string=? u "") (string=? v ""))
    0)
    ((char=? (string-ref t 0) (string-ref u 0) (string-ref v 0))
    (+ 1 (llcs3 (substring t 1) (substring u 1) (substring v 1))))
    (else
    (max (llcs3 (substring t 1) u v)
    (llcs3 t (substring u 1) v)
    (llcs3 t u (substring v 1))))
    )))s
     */

    public int llcs3 (String v, String u, String t){
        if( v.equals("") || u.equals("") || t.equals("")){
            return 0;
        }else{
            if ((v.charAt(0) == u.charAt(0)) && (v.charAt(0) == t.charAt(0))){
                return (1 + (llcs3 ((v.substring(1)),(u.substring(1)),(t.substring(1)))));
            }else{
                return Math.max(Math.max((llcs3((v.substring(1)),u ,t)), (llcs3(v,(u.substring(1)),t))), (llcs3(v, u,(t.substring(1)))) );
            }
        }
    }

    public int llcs3DP(String v, String u, String t){
        int[] mem =new int[3];
        return mem.length;
    }

}
