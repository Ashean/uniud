import java.util.*;
/**
 * Aggiungi qui una descrizione della classe Towers
 * 
 * @author (il tuo nome) 
 * @version (un numero di versione o una data)

4. Definizione di classi Java
Un’istanza della classe Towers rappresenta lo stato del rompicapo della torre di Hanoi e permette di modellare
l’evoluzione del gioco, attraverso successive disposizioni valide di n dischi, tenendo traccia delle mosse effettuate. I
dischi sono numerati da 1 a n in ordine crescente di diametro; le tre asticciole previste dal rompicapo sono identficate
da 1, 2 e 3. Il protocollo della classe è specficato come segue:

• public Towers( int n )
costruttore, il cui parametro n indica il numero di dischi, ovvero l’altezza della torre da ricostruire.

• public void put( int disk, int rod )
metodo per stabilire la posizione iniziale del disco disk in corrispondenza all’asticella rod, prima di giocare.

• public void move( int disk, int dst )
metodo che consente di modi care lo stato del gioco effettuando una mossa che sposta il disco disk dall’asticella
in cui si trova all’asticella di destinazione dst.

• public int height()
metodo che restituisce l’altezza della torre da ricostruire (o equivalentemente il numero di dischi del gioco).

• public int site( int disk )
metodo che restituisce il numero che identifica l’asticella in corrispondenza alla quale è collocato il disco disk.

• public int transit( int disk, int dst )
metodo che restituisce il numero che identifica l’asticella di transito per il disco disk con destinazione dst:
l’asticella di transito è quella che rimane escludendo l’asticella in cui è collocato disk e l’asticella dst.

• public String moves()
metodo per acquisire la stringa che codi ca la sequenza di mosse effettuate.

Nella stringa restituita da moves, una mossa dall’asticella src all’asticella dst è codi cata da cinque caratteri: uno spazio
bianco, seguito dalla cifra che denota src, seguita dalla coppia di caratteri "->" e dalla la cifra che denota dst. Inoltre, le
codi che di mosse successive sono semplicemente giustapposte. Per esempio, le 7 mosse che risolvono il rompicapo
per tre dischi, inizialmente tutti collocati in corrispondenza all’asticella 1, ricostruendo la torre in corrispondenza
all’asticella 2 sono rappresentate dalla stringa:
" 1->2 1->3 2->3 1->2 3->1 3->2 1->2"

La classe Towers può essere impostata in Java come segue:
 **/

public class Towers {
        // Variabili di istanza ...
    
    private int num;  //registra il numero dei dischi
    private int[] where; //registra i dischi
    private String mvs; //registra le mosse
    
    
    // Costruttore ...
    public Towers(int n) {
        num = n;
        where = new int[ n+1 ]; //tanti quanti sono i dischi, i dischi sono numerati a partire da 1 quindi usiamo il +1
        mvs = "";
        
    }

    // Metodi ...
    public void put(int disk, int rod) {
        where[disk] = rod; //assegno il bastone al disco
    }

    public void move(int disk, int dst) {
        
        mvs = mvs + " " + where[disk] + "->" + dst;
        where[disk] = dst;
    }

    public int height() {
        return num; //se metto i dischi tutti su un bastone, è il numero di dischi!
    }

    public int site(int disk) {
        return where[disk]; //accedo all'array where con il numero del disco
    }

    public String moves() {
        return mvs;  
    }

    public int transit( int disk, int dst ) {
        return ( 6 - site(disk) - dst );
    }
} // class Towers
