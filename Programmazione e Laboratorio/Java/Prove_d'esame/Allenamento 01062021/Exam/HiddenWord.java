
/**
 * Aggiungi qui una descrizione della classe hIDD
 * 
 * @author (il tuo nome) 
 * @version (un numero di versione o una data)
 */
public class HiddenWord

{
    // variabili d'istanza - sostituisci l'esempio che segue con il tuo
    private String x;
    private int len;
    /**
     * Costruttore degli oggetti di classe  hIDD
     */
    public HiddenWord(String hw)
    {

        x = hw;
        len = x.length();
    }

    public int length(){
        return len;
    }

    public String guess (String gw){
        String rev= "";
        int min_len = Math.min(gw.length(), len);
        for(int i = 0; i < min_len ; i++){
            if(x.charAt(i)==gw.charAt(i)){
                rev = rev + x.charAt(i);
            }else{
                rev = rev + "*";
            }
        }
        for(int i = min_len; i < len; i++){
            rev = rev + "*";
        }
        return rev;
    }

    public static long q( int i, int j, boolean b ) { // i, j >= 0
        if ( b ) {
            if ( i*j == 0 ) {
                return i + j + 1;
            } else {
                return q( i-1, j, b ) + q( i, j-1, b ) + q( i, j, !b );
            }
        } else {
            if ( i*j == 0 ) {
                return 1;
            } else {
                return q( i-1, j, b ) + q( i, j-1, b );
            }}
    }

    /**
     *Considera il metodo statico q definito sopra. Trasforma il programma ricorsivo in un programma iterativo applicando
    opportunamente la tecnica bottom-up di programmazione dinamica.
     */

    public static long qDP( int i, int j, boolean b ) { // i, j >= 0
        long[][][] h = new long[ i+1 ][ j+1 ][ 2 ];
        for ( int v=0; v<=j; v=v+1 ) {
            h[0][v][0] = 1;
            h[0][v][1] = v + 1;
        }
        for ( int u=1; u<=i; u=u+1 ) {
            h[u][0][0] = 1;
            h[u][0][1] = u + 1;
        }
        for ( int u=1; u<=i; u=u+1 ) {
            for ( int v=1; v<=j; v=v+1 ) {
                h[u][v][0] = h[u-1][v][0] + h[u][v-1][0];
                h[u][v][1] = h[u-1][v][1] + h[u][v-1][1] + h[u][v][0];
            }}
        if ( b ) {
            return h[i][j][1];
        } else {
            return h[i][j][0];
        }
    }
}
