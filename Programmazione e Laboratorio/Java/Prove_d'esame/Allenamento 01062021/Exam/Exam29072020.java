
/**
 * Aggiungi qui una descrizione della classe NoveLuglioVentiVenti
 * 
 * @author (il tuo nome) 
 * @version (un numero di versione o una data)
 */
public class Exam29072020
{
    // variabili d'istanza - sostituisci l'esempio che segue con il tuo
    /**
     * 3. Memoization
    Considera la seguente procedura funzionale (metodo statico), basata su una ricorsione ad albero:
     */
    public static long rec( int n, int k ) { // 0 ≤ k ≤ n
        if ( (n-k) * k <= 0 ) {
            return 1;
        } else {
            return rec( n-1, k ) + rec( n-1, n-k );
        }
    }

    /**3.1. Supponi che nel corso dell’esecuzione di un programma che utilizza rec venga valutata l’espressione:
    rec( 10, 4 )
    Questa valutazione si svilupperà attraverso successive invocazioni ricorsive 
    di rec(n,k) per diversi valori degli
    argomenti n e k. Quali sono il valore più piccolo e il valore più grande che assumerà ciascuno degli argomenti nelle
    ricorsioni che discendono da rec(10,4) ?
    • Valore più piccolo di n : k  e valore più grande di n : n;
    • Valore più piccolo di k : 0   e valore più grande di k : k.
    3.2.
    Applica una tecnica top-down (ricorsiva) di memoization per realizzare la procedura rec in modo più efficiente
     */
    public static long recMem(int n, int k){
        long [][] mem =new long[n+1][k+1];

        for(int i =0; i<= n;i++){
            for(int j=0; j<=k; j++){
                mem[i][j] = 0;
            }
        }

        return recRec(n, k, mem);
    };

    private static long recRec(int n, int k, long[][] mem){
        for(int r=0; r<=n;r++){
            for(int c=0; c<= k; c++){
                if ( (n-k) * k <= 0 ) {
                    mem[n][k]= 1;
                } else {
                    mem[n][k]= recRec( n-1, k ,mem) + recRec( n-1, n-k ,mem);
                }
            }
        }
        return mem[n][k];
    }

    /**
     * 4. Correttezza dei programmi iterativi
    In corrispondenza al programma iterativo che realizza la procedura f sono riportate precondizione (Pre),
    postcondizione (Post), invariante (Inv) e funzione di terminazione (Term). In particolare, si intende che l’invariante e la
    funzione di terminazione consentono di dimostrare la correttezza completa del programma in relazione a quanto
    specificato da precondizione e postcondizione.

    4.1. Completa la definizione di f in modo che le proprietà formalizzate nell’invariante siano soddisfatte e che
    l’espressione associata a Term definisca effettivamente una funzione di terminazione.
     */
    public static int f( int n ) {    // Pre: n > 0
        int x = 1;
        int y = 1;
        int z = 1;
        while ( x + y <= n ) {       // Inv: ∃k . x=2^k  ∧  1 ≤ y ≤ x   ∧  x+y ≤ n+1  ∧  z+1 = 2y  
                                     // Term: n+1 – x – y
            if ( x == y ) {
                x = 2*x;
                y = 1;
                z = 1;
            } else {
                y = y + 1;
                z = z + 2;
            }}
        return z;                   // Post: ∃k . x=2^k ∧ 2x > n   ∧   1 ≤ y ≤ x   ∧  x+y = n+1   ∧   z+1 = 2y  
    }
    /**
    4.2. Dimostra che al termine dell’iterazione le proprietà specificate dalla postcondizione (Post) sono una conseguenza
    logica di quelle relative all’invariante (Inv).
    
    a.L' Inv vale all' inizio
         Pre ==> Inv(1,A,B)
         
         Inv: ∃k . x=2^k  ∧  1 ≤ y ≤ x   ∧  x+y ≤ n+1  ∧  z+1 = 2y
         Inv: ∃k . 1=2^k  ∧  1 ≤ A ≤ 1   ∧  1+A ≤ n+1  ∧  B+1 = 2A
         
         Allora k = 0 e A = 1
         
         Inv: ∃k . k=0  ∧  1 ≤ A ≤ 1   ∧  1+1 ≤ n+1  ∧  B+1 = 2
         Inv: ∃k . k=0  ∧  1 ≤ A ≤ 1   ∧  n >= 1  ∧  B = 1
        
        n=> 1 e quindi Pre condizione verificata.... x = 1, y = A = 1, z = B = 1
        
    b.L'Inv si conserva
        Inv && ( x + y ≤ n) ==> Inv'
        
        // Assumo : ∃k . x=2^k  ∧  1 ≤ y ≤ x   ∧  x+y ≤ n+1  ∧  z+1 = 2y ed inoltre ( x + y ≤ n)
        Dimostro :  Inv: ∃k . x'=2^k  ∧  1 ≤ y' ≤ x'   ∧  x'+y' ≤ n+1  ∧  z'+1 = 2y'
        
        
        b.1 Assumo inoltre che ( x == y) 
            Inv(x',y',z') = Inv(x',1,z') 
            
                Dimostro :Inv: ∃k . x'=2^k  ∧  1 ≤ y' ≤ x'   ∧  x'+y' ≤ n+1  ∧  z'+1 = 2y'
                          Inv: ∃k . x'=2^k  ∧  1 ≤ y' ≤ x'   ∧  x'+y' ≤ n+1  ∧  z'+1 = 2
                          Inv: ∃k . x'=2^k  ∧  1 ≤ 1 ≤ x'   ∧  x'+1 ≤ n+1  ∧  z' = 1
                          Inv: ∃k . x'=2^k  ∧  1 ≤ 1 ≤ x'   ∧  x' ≤ n  ∧  z' = 1
                          Inv: ∃k . x'=2^k  ∧   x' >= 1   ∧  x' ≤ n  ∧  z' = 1
                        
               Ho trovato z' = 1, e   1 <= x' <= n, x' deve essere una pot di 2 
             
            
        b.2 Assumo inoltre che ( x != y)
               Inv(x',y',z') = Inv(x,y+1,z+B)
               
               Dimostro :Inv: ∃k . x'=2^k  ∧  1 ≤ y' ≤ x'   ∧  x'+y' ≤ n+1  ∧  z'+1 = 2y'
                        :Inv: ∃k . x'=2^k  ∧  1 ≤ y+1 ≤ x'   ∧  x'+y+1 ≤ n+1  ∧  z'+1 = 2(y+1)
                         Inv: ∃k . x'=2^k  ∧  1 ≤ y+1 ≤ x'   ∧  x'+y+1 ≤ n+1  ∧  z+B+1 = 2(y+1)
                         Inv: ∃k . x'=2^k  ∧  1 ≤ y+1 ≤ x   ∧  x+y ≤ n  ∧  B = 2(y+1) - z -1
                         Inv: ∃k . x'=2^k  ∧  1 ≤ y+1 ≤ x   ∧  x+y ≤ n  ∧  B = 2y -1 -z +2 
                         Inv: ∃k . x =2^k  ∧  1 ≤ y+1 ≤ x   ∧  x+y ≤ n  ∧  B = z - z +2 
                         Inv: ∃k . x =2^k  ∧  1 ≤ y+1 ≤ x   ∧  x+y ≤ n  ∧  B = 2               OK!
                          
    c.Verifica della Postcondizione
        Inv && ( x + y > n) ==> Post
            ∃k . x=2^k  ∧  1 ≤ y ≤ x   ∧  x+y ≤ n+1  ∧  z+1 = 2y , x+y>n =>  
                             1 < n - x < x
                             x < n < 2x
        
    Term abbia valori naturali
        Inv(x,y,z) ==> n+1-y-x > 0
        n+1-x-y > 0 =>  x+y < n+1  Fin tanto che vale Inv
        

    Term è strettamente descrescente
        
        Inv(x,y,z) && ( x+y <= n) ==> term(x',y') < term(x,y)
            
        t1.) (x == y) && Inv 
            
            n+1-x'-y' < n+1-x-y
            n+1-x'- 1 < n+1-x-y
            -x'-1 < -x - y
            x' > x + y -1    siccome entriamo per x == y, allora x + y = 2x
            x' > 2x -1       consideriano che le x = 2^k 
            x' > 2*2^k -1
            x' > 2^(k+1) -1
            
            ∃k . x=2^k  ∧  1 ≤ y ≤ x   ∧  x+y ≤ n+1  ∧  z+1 = 2y 
            
        t2)  Inv(x',y',z') = Inv(x,y+1,z+2) 
            n+1-x'-y' < n+1-x-y
            n+1-x-y-1< n+1 -x -y
            n+1-x-y-1< n+1 -x -y
            -y-1< -y
            OK!
        
        
        
        
        
        
        
        
        
        
        */
}
