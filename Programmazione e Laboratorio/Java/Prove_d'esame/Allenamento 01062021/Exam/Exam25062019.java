import java.util.*;
/**
 * Aggiungi qui una descrizione della classe Exam25062019
 * 
 * @author (il tuo nome) 
 * @version (un numero di versione o una data)
 */
public class Exam25062019
{
    /**
     * 2.a Memoization
    Applica la tecnica top-down di memoization per realizzare una versione più efficiente del seguente programma, che
    risolve una variante del problema della sottosequenza comune più lunga (calcola la differenza fra due stringhe,
    restituendo le sequenze di caratteri rimossi rispettivamente da u e da v):
     **/  
    public static String[] diff( String u, String v ) {
        if ( u.equals("") || v.equals("") ) {
            return new String[] { u, v };
        } else if ( u.charAt(0) == v.charAt(0) ) {
            return diff( u.substring(1), v.substring(1) );
        } else {
            String[] x = diff( u.substring(1), v );
            String[] y = diff( u, v.substring(1) );
            if ( x[0].length() < y[0].length() ) {
                return new String[] { u.charAt(0)+x[0], x[1] };
            } else {
                return new String[] { y[0], v.charAt(0)+y[1] };
            }}
    }

    public static String[] diffMem(String u, String v){
        int n = u.length();
        int m = v.length();
        String [][][] mem = new String[n+1][m+1][];
        for(int i = 0; i <= n ; i++){
            for(int j = 0; j<=m ; j++){
                mem[i][j] = null;
            }
        }

        return diffRec( u, v , mem);
    }

    private static String[] diffRec(String u, String v, String[][][] mem){
        int n = u.length();
        int m = v.length();

        if ( u.equals("") || v.equals("") ) {
            mem[n][m]= new String[] { u, v };
        } else if ( u.charAt(0) == v.charAt(0) ) {
            mem[n][m] = diffRec( u.substring(1), v.substring(1), mem );
        } else {
            String[] x = diffRec( u.substring(1), v,mem );
            String[] y = diffRec( u, v.substring(1),mem );
            if ( x[0].length() < y[0].length() ) {
                mem[n][m] = new String[] { u.charAt(0)+x[0], x[1] };
            } else {
                mem[n][m] = new String[] { y[0], v.charAt(0)+y[1] };
            }}

        return mem[n][m];
    } // corretto al rpimo tent :)

    /**
     * 3. Ricorsione e iterazione
    Attraverso il metodo statico st, il programma ricorsivo seguente calcola i numeri di Stirling del II tipo (problema dei
    piatti e dei pasticcini). In particolare, è una variante della procedura ricorsiva standard, che utilizza una variabile di
    stato come contatore. Trattandosi di una variabile condivisa da tutte le invocazioni ricorsive, questa deve essere passata
    per riferimento, cosa possibile in Java solo con oggetti; pertanto il contatore viene rappresentato da un array di un solo
    elemento. Completa la definizione del metodo stIter che trasforma la ricorsione in iterazione applicando uno stack.**/

    public static long st( int n, int k ) { // n, k > 0
        long[] ct = new long[] { 0 }; // contatore: variabile di stato
        sRec( n, k, 1, ct );
        return ct[0];
    }

    private static void sRec( int n, int k, int q, long[] ct ) {
        if ( (k == 1) || (k == n) ) {
            ct[0] = ct[0] + q;
        } else {
            sRec( n-1, k-1, q, ct );
            sRec( n-1, k, k*q, ct );
        }
    }

    public static long stMem(int n, int k){
        long[] ct = new long[] { 0 };
        Stack<long[]> stk = new Stack<long[]>();
        stk.push(new long[] {n , k, 1});

        while( !stk.empty() ){
            long [] f = stk.pop(); 
            long a = f[0];
            long b = f[1];
            long q = f[2];
            if ( (b == 1) || (b == a) ) {
                ct[0] = ct[0] + q;
            } else {
                stk.push(new long[]{ a-1, b, b*q} );
                stk.push(new long[]{ a-1, b-1, q} );         
            }
        }

        return ct[0];
    }
}
