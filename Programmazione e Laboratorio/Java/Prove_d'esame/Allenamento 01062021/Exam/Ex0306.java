
/**
 * Aggiungi qui una descrizione della classe Ex0306
 * 
 * @author (il tuo nome) 
 * @version (un numero di versione o una data)
 */
public class Ex0306
{
    /**
     * 5. Verifica formale della correttezza
    Il programma iterativo per josephus, che deriva dalla procedura ricorsiva (ufo) discussa a lezione, risolve il problema
    ispirato da un racconto di Giuseppe Flavio quando il numero di commensali (≥ 1) è rappresentato in forma binaria da
    una stringa x. Tale stringa deve quindi contenere solo le cifre 0/1 e deve iniziare con 1 (cifra più significativa). Nel
    programma sono riportati precondizione, che si riferisce allo stato immediatamente dopo l’introduzione della costante n,
    postcondizione e invariante. Per comodità si è utilizzata la seguente notazione: xj
    denota il valore della cifra in posizione j nella stringa x, cioè xj= 0 se x.charAt(j) è '0' e xj
    = 1 se x.charAt(j) è '1'; ∀ j significa per tutte le posizioni ammissibili nella stringa x; ∑ j ∈ [1,i–1]
    è la sommatoria estesa a tutti i valori interi di j da 1 a i–1.
    Dimostra formalmente la correttezza parziale (cioè tralasciando la terminazione) del programma */

    public static int josephus( String x ) {
        final int n = x.length(); // Pre: n > 0, ∀ j . xj ∈ [0, 1], x0= 1
        int k = 1;
        int i = 1;
        while ( i < n ) { // Inv: 0 < i ≤ n, k = 2 ( ∑j ∈ [1,i–1]  xi–j 2j–1 ) + 1
            if ( x.charAt(i) == '0' ) { // se xi= 0 …
                k = 2 * k - 1;
            } else {
                k = 2 * k + 1;
            }
            i = i + 1;
        }
        return k; // Post: k = 2 ( ∑j ∈ [1,n–1]xn–j 2j–1 ) + 1
    }
    /**
     * Il nostro inviarente si divide in 2 parti:
     *           
     *           Inv: 0 < i ≤ n, k = 2 ( ∑j ∈ [1,i–1]  x_(i–j) 2^(j–1) ) + 1
     *
     * Espandendo la sommatoria:
     *
     *   Inv: 0 < i ≤ n, k = 2 ( x_(i–1)* 2^(1–1) + x_(i–2)* 2^(2–1)+ ... +  x_(i–(i-1))* 2^((i-1)–1)) + 1
     *   Inv: 0 < i ≤ n, k = 2 ( x_(i–1) + x_(i–2)* 1/2 + ... +  x_(-1)* 2^((i-2)) + 1
     * 
     * a. Vediamo cosa vale l'invariante all' inizio:
     * 
     *    k = 1 quando
     *    Inv: 0 < i ≤ n, 1 = 2 ( x_(i–1)* 2^(1–1) + x_(i–2)* 2^(2–1)+ ... +  x_(i–(i-1))* 2^((i-1)–1)) + 1
     *
     *    i = 1 
     *    Inv: 0 < i ≤ n, i = 2 ( x_(1–1)* 2^(1–1) + x_(1–2)* 2^(2–1)+ ... +  x_(i–(i-1))* 2^((1-1)–1)) + 1
     *    Inv: 0 < 1 ≤ n, 1 = 2 ( x_(0)* 2^(0) + x_(-1)* 2^(-1)+ ... +  x_(1)* 2^(–1 ) + 1
     *    Inv: 0 < 1 ≤ n, 1 = 2 ( somma vuota ) + 1  Ok
     *
     * b. Conservazione dell' invariante
     *    Assumo: Inv & ( i < n )
     *    
     *    0 < i'≤ n
     *    k' = 2 ( x_(i'–1) + x_(i'–2)* 1/2 + ... +  x_(-1)* 2^((i'-2)) + 1
     *     
     *    dove i' = i+1 
     *     
     *    0 < i+1 ≤ n
     *     0 < i+1 ≤ n, k = 2 ( x_(i+1–1)* 2^(1–1) + x_(i+1–2)* 2^(2–1)+ ... +  x_(i+1–(i+1-1))* 2^((i+1-1)–1)) + 1
     *     0 < i+1 ≤ n, k = 2 ( x_(i)* 2^(1–1) + x_(i–1)* 2^(2–1)+ ... +  x_(+1)* 2^(i–1) + 1
     * 
     *    Poi k può variare in 2k+1 oppure 2k-1 quindi:
     *    
     *      0 < i+1 ≤ n, i+1 è sicuramente manggiore di 0 e per la condizione del while i+1<n quindi VERO
     *    
     *     2k +/-1 = 2 ( x_(i)* 2^(1–1) + x_(i–1)* 2^(2–1)+ ... +  x_(+1)* 2^(i–1) + 1
     *     
     *     
     *     Isolo il primo elemento poiché gli x e j che estraggo sono gli stessi della mia assunzione:
     *    
     *     2k +/-1 = 2( x_(i)* 2^(1–1)) +  2( x_(i–1)* 2^(2–1)+ ... +  x_(+1)* 2^(i–1) + 1
     *  
     *     Raccolgo 2 nella sommatoria e diminuisco tutti i pesi di due a 1 in modo tale da farlo assomigliare all' invariante
     *     
     *     2k +/-1 = 2( x_(i)* 2^(1–1)) +  2*2( x_(i–1)* 2^(1–1)+ ... +  x_(+1)* 2^(i–2)) + 1
     *  
     *     
     *     Noto infatti che 
     *     2( x_(i–1)* 2^(1–1)+ ... +  x_(+1)* 2^(i–2)) è uguale alla parte di sommatoria dell'invariante
     *     
     *     Nell' invariante abbiamo che k = somatoria +1
     *     quindi avremo che: sommatoria = k - 1, posso dunque effettuare una sostituzione
     *     
     *     2k +/-1 = 2( x_(i)* 2^(1–1)) +  2*(k-1) + 1 
     *     
     *     Sviluppiamo il secondo membro dell' uguaglianza
     *     2( x_(i)* 2^(1–1)) +  2*(k-1) + 1 = 2k - 1 + 2 x_i
     *     
     *     Alla fine avremmo che
     *     
     *     2k +/-1 = 2k - 1 + 2 x_i
     *     
     *     Per decidere se applichiamo più o meno nel K, dobbiamo specificare i due rami dell' if
     *     
     *       b1. Se inoltre x_i = 0, allora
     *           
     *           2k - 1 = 2k - 1 + 2*(0)   VERO
     *           
     *       b2. Se inoltre x_i = 1, allora:
     *        
     *           2k + 1 = 2k + 2 - 1 = 2k +1 VERO
     *           
     * c. Assumo: Inv & (i >= n)     
     *     
     *       i = n, k = 2 ( x_(n–1)* 2^(1–1) + x_(n–2)* 2^(2–1)+ ... +  x_(n–(n-1))* 2^((n-1)–1)) + 1
     *      Notiamo però che quel che abbiamo scritto è esattamente equivalente alla post condizione quindi: OK   
     *     
     */

    /**
     * 3. Programmazione in Java
    Gli argomenti del metodo statico commonStretches sono due stringhe u, v composte solo dai caratteri '0' e '1',
    entrambe con esattamente m occorrenze di '0' ed n occorrenze di '1'. Tali stringhe rappresentano due percorsi di
    Manhattan in un reticolo di m x n tratti di strada, dove '0' denota uno spostamento verticale e '1' orizzontale. Date le
    stringhe u, v, il metodo commonStretches calcola il numero di tratti di strada comuni ai due percorsi, numero che
    può variare da 0 a m+n, nel secondo caso quando i due percorsi (e quindi le due stringhe) coincidono. Per esempio:
    commonStretches( "1010110110", "1100011101" ) → 3 // m = 4, n = 6
    Affinché un certo tratto sia comune a due percorsi, questi devono passare per lo stesso nodo (incrocio) e lo spostamento
    successivo deve chiaramente procedere nella stessa direzione (orizzontale o verticale). I due percorsi passano per lo
    stesso nodo se una parte iniziale (prefisso) delle stringhe che li rappresentano è composta dallo stesso numero di '0' e
    dallo stesso numero di '1'; inoltre, lo spostamento che segue va nella stessa direzione se il carattere successivo delle
    due stringhe coincide. Nell’esempio questa situazione si verifica in corrispondenza ai caratteri in posizione 0, 3 e 7 come illustrato dalla figura qui a lato.
    Definisci in Java il metodo statico commonStretches in accordo con le specifiche indicate.
     */

    //Versione mia
    public static int commonStretches (String u, String v){
        int n = u.length();

        int count = 0;
        int uZeri = 0;
        int vZeri = 0;

        for(int k=0; k<n; k++){
            if((uZeri == vZeri) && (u.charAt(k) == v.charAt(k))){
                count ++;
            }if(u.charAt(k) == '0'){
                uZeri++;
            }
            if(v.charAt(k) == '0'){
                vZeri++;
            }
        }
        return count;
    }

    /**
     * 3. Programmazione dinamica
    Applica la tecnica bottom-up di programmazione dinamica per realizzare una versione più efficiente del metodo statico
    q riportato qui di seguito:
     **/

    public static long q( int i, int j, int k ) { // i, j, k >= 0

        long x = ( i < 2 ) ? i : q( i-2, j, k );
        long y = ( j < 2 ) ? j : q( i, j-2, k );
        long z = ( k < 2 ) ? k : q( i, j, k-2 );
        long m = x + y + z;

        return ( m == 0 ) ? 1 : m;
    }

    public static long qDP( int i, int j, int k ) { // i, j, k >= 0
        long [][][] mem = new long[i+1][j+1][k+1];

        for(int r = 0; r <= i; r++){
            for(int c = 0; c <= j; c++){
                for(int t = 0; t <= k; t++){

                    long x = ( r < 2 ) ? r : mem[ r-2][ c][ t ];
                    long y = ( c < 2 ) ? c : mem[ r][ c-2][ t ];
                    long z = ( t < 2 ) ? t : mem[ r][ c ][ t-2 ];
                    long m = x + y + z;
                    
                    mem[r][c][t]=( m==0)?1 : m;
                }
            }
        }

        return mem[i][j][k];
    }
}
