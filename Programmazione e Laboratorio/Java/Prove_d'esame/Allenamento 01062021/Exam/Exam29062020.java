import java.util.*;
/**
 * Aggiungi qui una descrizione della classe Exam26092020
 * 
 * @author (Ashean Abeysinghe) 
 * @version (u0.0)
 */
public class Exam29062020
{
    // variabili d'istanza - sostituisci l'esempio che segue con il tuo

    /**
     * 2. Programmazione dinamica
    Considera la seguente procedura funzionale (metodo statico), basata su una ricorsione ad albero:**/

    public static long rec( int x, int y, int z ) { // 1 <= x, y <= z
        if ( (x == 1) || (y == z) ) {
            return 1;
        } else {
            return rec( x-1, y, z ) + x * rec( x, y+1, z );
        }
    }

    /**2.1. Supponi che nel corso dell’esecuzione di un programma che utilizza rec venga valutata l’espressione:

     * rec( 8, 5, 12 )

     * Questa valutazione si svilupperà attraverso successive invocazioni ricorsive di rec(x,y,z) per diversi valori degli
     * argomenti x, y e z. Quali sono il valore più piccolo e il valore più grande che assumerà ciascuno degli argomenti
    nelle ricorsioni che discendono da rec(8,5,12) ?

    • Valore più piccolo di x : 1    e valore più grande di x : x;
    • Valore più piccolo di y : y    e valore più grande di y : z;
    • Valore più piccolo di z : z    e valore più grande di z : z.

    2.2. Applica una tecnica di programmazione dinamica bottom-up (iterativa) per realizzare una versione più efficiente
    della procedura rec.*/

    public static long recMem(int x, int y, int z){
        long [][] mem = new long[ x+1 ][ z+1 ];
        for(int i = 1; i <= x; i++){
            for(int j = z ; j>= y; j--){                
                if ( (i == 1) || (j == z) ) {
                    mem[i][j] = 1;
                } else {
                    mem[i][j] = mem[i-1] [j] + i * mem[i][j+1] ;
                }
            }
        }

        return mem[x][y];
    }

    /**
     * 3. Ricorsione e iterazione
    Il seguente programma, basato sulla ricorsione, risolve il rompicapo della torre di Hanoi a partire da una qualsiasi
    disposizione valida dei dischi in corrispondenza alle tre asticelle, e restituisce la sequenza di mosse codificata da una
    stringa. Per tenere traccia dello stato del gioco viene utilizzato un oggetto di tipo Towers, che inizialmente rappresenta
    la con configurazione da cui si intende partire e successivamente, attraverso il metodo moves, permette di acquisire la
    stringa che rappresenta le mosse effettuate. (Ai ni di questo esercizio non è comunque necessario conoscere le
    speci che del protocollo di Towers).*/

    public static String hanoi( Towers hts, int d ) { // hts: stato iniziale gioco // d: posizione finale torre
        hanoiRec( hts.height(), d, hts );
        return hts.moves();
    }

    private static void hanoiRec( int n, int d, Towers hts ) {
        if ( n > 0 ) {
            if ( hts.site(n) == d ) {
                hanoiRec( n-1, d, hts );  //se il disk più grosso è già nella destinazione pèrosegui
            } else {  //altrimenti sposta i disci per mettere quello più grosso nella torre di dest
                int t = hts.transit( n, d );    // numero asticella di supporto per la mossa
                hanoiRec( n-1, t, hts );        //sposto tutto nella asticella di supporto
                hanoiRec( -n, d, hts );         //sposto tutto nella asticella di destinazione
                hanoiRec( n-1, d, hts );        // continuo la ricorsione
            }
        } else if ( n < 0 ) {
            hts.move( -n, d );                  //spostamento 
        }
    }

    /**
     * 3.1. Completa la definizione del metodo hanoiIter che trasforma la ricorsione in iterazione applicando uno stack.
     */
    public static String hanoiIter( Towers hts, int d ) {
        int n = hts.height();
        Stack<int[]> stk = new Stack<int[]>();
        stk.push( new int[]{ n, d } );
        while ( ! stk.empty() ) { // fin tanto che lo stack non è vuoto
            int[] f = stk.pop();  // prendi l' elemento in cima allo stack ed elabora segunetemente : 
            n = f[0];
            d = f[1];
            if ( n > 0 ) {
                if ( hts.site(n) == d ) {
                    stk.push( new int[]{n - 1, d } );
                } else {
                    int t = hts.transit( n, d );
                    stk.push(new int[]{n-1,d});
                    stk.push(new int[]{-n,d});
                    stk.push(new int[]{n-1,t});;
                }
            } else if ( n < 0 ) {
                hts.move(-n, d);
            }
        }
        return hts.moves();
    }
    
    /**
     * 3.2. In base al codice di hanoiRec, il parametro n può anche assumere valori negativi. Quale potrebbe esserne la
        funzione, a tuo giudizio? Spiega brevemente la tua interpretazione.
        
        Fin tanto che n è positivo indica il numero di dischi rimanenti/il peso del successivo disco da iterare, mentre quando è negativo, poiché serve solo
        ad accedere alla funzione hts.move, evidententemente assume un ruolo di contatore delle mosse rimanenti.
     */
    
    
}
