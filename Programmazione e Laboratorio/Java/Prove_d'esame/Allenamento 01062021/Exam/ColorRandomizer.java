import java.awt.*;
/**
 * Aggiungi qui una descrizione della classe ColorRandomizer
 * 
 * @author (il tuo nome) 
 * @version (un numero di versione o una data)
 */
public class ColorRandomizer
{
    //Creiamo un array di color
    private Color[] collection;
    private int len;
    /**
     * Costruttore degli oggetti di classe  ColorRandomizer
     */
    public ColorRandomizer()
    {
        // inizializza le variabili d'istanza
        this.len = 0;
        this.collection = new Color[len];
    }
    
    public int size(){
        return this.len;
    }
    
    public void add (Color c){
        Color[] new_collection = new Color[len+1];
        
        for(int i = 0; i < len ; i++){
            new_collection[i] = this.collection[i];
        }
        new_collection[len] = c;
        this.collection = new_collection;
        this.len = this.len + 1;
    }
    
    public Color pick(){
        int random =(int) Math.random()*(len-1);
        return collection[random];
    }
    

    public Color extract(){
        Color[] new_collection = new Color[len-1];
        int random =(int) Math.random()*(len-1);
        
        for(int i = 0; i < random; i++){
                new_collection[i] = this.collection[i];
            } 
        for(int i = random+1; i < this.size()-1; i++){
                new_collection[i] = this.collection[i];
            } 
            this.collection = new_collection;
            len = len -1;
        return this.collection[random];
    }
}
