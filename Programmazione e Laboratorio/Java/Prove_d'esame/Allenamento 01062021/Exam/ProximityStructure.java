
public class ProximityStructure {

    public  double [] collection;
   
    public ProximityStructure() {
        this.collection = new double[0];
       
    }

    public int size() {
        return this.collection.length;
    }

    public void add( double x ) {

        double [] new_collection = new double [this.size()+1]; 
        if( this.size() != 0){
            for(int i = 0; i < this.size(); i++){
                new_collection[i] = this.collection[i];
            }       
        }
         new_collection[this.size()] = x;
            this.collection = new_collection;
    }

    public double removeClosestTo( double x ) {
        double diff = Math.abs(this.collection[0] - x);
        double [] new_collection = new double [this.size()-1];
        int index= 0;
        for(int i = 0; i< this.size(); i++){
            if(diff > Math.abs(this.collection[i] - x)){
                diff = Math.abs(this.collection[i] - x);
                index = i;
            }
        }
        for(int i = 0; i < index; i++){
                new_collection[i] = this.collection[i];
            } 
        for(int i = index+1; i < this.size()-1; i++){
                new_collection[i] = this.collection[i];
            } 
            this.collection = new_collection;
        return this.collection[index];
    }
} // class ProximityStructure�