import java.util.*;
/**
 * Aggiungi qui una descrizione della classe Ex
 * 
 * @author (Ashean Abeysinghe) 
 * @version (Esercitazione temi d'esame de 01/06/2021)
 */
public class Ex01060
{
    /**
     *
    2.a Memoization
    Applica la tecnica top-down di memoization per realizzare una versione più efficiente del seguente programma, che
    risolve una variante del problema della sottosequenza comune più lunga (calcola la differenza fra due stringhe,
    restituendo le sequenze di caratteri rimossi rispettivamente da u e da v):
     **/

    public static String[] diff( String u, String v ) {
        if ( u.equals("") || v.equals("") ) {
            return new String[] { u, v };
        } else if ( u.charAt(0) == v.charAt(0) ) {
            return diff( u.substring(1), v.substring(1) );
        } else {
            String[] x = diff( u.substring(1), v );
            String[] y = diff( u, v.substring(1) );
            if ( x[0].length() < y[0].length() ) {
                return new String[] { u.charAt(0)+x[0], x[1] };
            } else {
                return new String[] { y[0], v.charAt(0)+y[1] };
            }}
    }

    public static String[] diffMem(String u, String v){

        int m = u.length();
        int n = v.length();

        String[][][] mem = new String[ m+1 ][ n+1 ][]; //Java ammette di inizializzare soltanto alcuni array

        for(int i = 0; i<=m; i++){
            for(int j=0; j<=n; j++){
                mem[i][j]= null;
            }
        }

        return diffRec(u, v, mem);
    }

    private static String[] diffRec(String u, String v, String[][][] mem){
        int m = u.length();
        int n = v.length();

        if(mem[m][n] == null){
            if ( u.equals("") || v.equals("") ) {
                mem[m][n] = new String[] { u, v };
            } else if ( u.charAt(0) == v.charAt(0) ) {
                return diffRec( u.substring(1), v.substring(1), mem );
            } else {
                String[] x = diffRec( u.substring(1), v, mem );
                String[] y = diffRec( u, v.substring(1), mem );
                if ( x[0].length() < y[0].length() ) {
                    mem[m][n]= new String[] { u.charAt(0)+x[0], x[1] };
                } else {
                    mem[m][n]= new String[] { y[0], v.charAt(0)+y[1] };
                }}
        }
        return mem[m][n];
    }

    /**
     *  3.b Ricorsione e iterazione
    Attraverso il metodo statico st, il programma ricorsivo seguente calcola i numeri di Stirling del II tipo (problema dei
    piatti e dei pasticcini). In particolare, è una variante della procedura ricorsiva standard, che utilizza una variabile di
    stato come contatore. Trattandosi di una variabile condivisa da tutte le invocazioni ricorsive, questa deve essere passata
    per riferimento, cosa possibile in Java solo con oggetti; pertanto il contatore viene rappresentato da un array di un solo
    elemento. Completa la definizione del metodo stIter che trasforma la ricorsione in iterazione applicando uno stack.�   
     */

    public static long st( int n, int k ) { // n, k > 0
        long[] cn = new long[] { 0 }; // contatore: variabile di stato
        sRec( 1, n, k, cn );
        return cn[0];
    }

    private static void sRec( int p, int n, int k, long[] cn ) {
        if ( (k == 1) || (k == n) ) {
            cn[0] = cn[0] + p;
        } else {
            sRec( p, n-1, k-1, cn );
            sRec( k*p, n-1, k, cn );
        }
    }

    public static long stIter( int n, int k ) { // n, k > 0
        long[] cn = new long[] { 0 };

        Stack<int[]> stack = new Stack<int[]>();
        int[] f = new int[] {1,n,k };

        stack.push( f );

        while (!stack.empty() ) {
            //fin tanto che lo stack non è vuoto
            //prendi l'elemento in cima allo stack ed itera
            f = stack.pop();

            if ( (f[2] == 1) || ( f[2] == f[1]) ) {
                cn[0] = cn[0] + f[0];
            } else {
                stack.push( new int[] { f[0]*f[2], f[1]-1, f[2]} );
                stack.push( new int[] { f[0], f[1]-1, f[2]-1 });
            }
        }
        return cn[0];
    }

    /**
     * 4. Verifica formale della correttezza
    Dato un intero n ≥ 0, il seguente metodo statico calcola la parte intera della radice quadrata di n utilizzando solo somme
    e confronti. Nel programma sono riportate precondizione, postcondizione e invariante. Introduci opportune espressioni
    negli spazi previsti in modo tale che i valori assunti dalle variabili soddisfino le relazioni specificate dalle asserzioni.
    Proponi inoltre una opportuna espressione che definisca i valori della funzione di terminazione.�

    Inv : 0 ≤ q ≤ √n , x = q^2, y = 2q+1, y+z = n

    a) Affinché l' invariante valga all' inizio del ciclo:

    Inv : 0 ≤ q^2 ≤ √n , x = q^2, y = 2q+1, y+z = n
    Inv : 0 ≤ 0 ≤ √n , 0 = q^2, y = 2*0+1, y+z = n
    Inv : 0 ≤ √n , x = q, y = 1, z = n-1

    b) L' invariante si conserva:

    Assumo : Inv : 0 ≤ q^2 ≤ √n , x = q^2, y = 2q+1, y+z = n, x<=z
    Dimostro : 0 ≤ q' ≤ √n' , x' = q'^2, y = 2q'+1, y'+z' = n

    - sostituico q' con q= q+1
    0 ≤ (q+1)^2 ≤ √n , x' = (q+1)^2, y = 2(q+1)+1, y'+z' = n

    - sostituico y' con  y=y+2
    0 ≤ (q+1)^2 ≤ √n , x' = (q+1)^2, (y+2) = 2(q+1)+1, (y+2)+z' = n

    Non conosco i  valori di x' e z', per conoscerli devo sfruttare al meglio :

    0 ≤ (q+1)^2 ≤ √n , x' = (q+1)^2, (y+2) = 2(q+1)+1, (y+2)+z' = n

    - sviluppo (q+1)^2
    0 ≤ (q^2)+2q+1 ≤ √n , x+A = q^2 + 2q + 1, (y+2) = 2(q+1)+1, (y+2)+z-B = n

    - sviluppo vedo che x = q^2 quindi sempilifico e 2q+1 = y
    0 ≤ (q+1)^2 ≤ √n , A =  y, (y+2) = 2(q+1)+1, (y+2)+z-B = n

    - sapendo che y + z = n :
    0 ≤ (q+1)^2 ≤ √n , A =  y, (y+2) = 2(q+1)+1, (y+2)+z-B = n
    0 ≤ (q+1)^2 ≤ √n , A =  y, (y+2) = 2(q+1)+1, y+z-B+2 = n
    0 ≤ (q+1)^2 ≤ √n , A =  y, (y+2) = 2(q+1)+1, n-B+2 = n
    0 ≤ (q+1)^2 ≤ √n , A =  y, (y+2) = 2(q+1)+1, B = 2

    c) L' invariante si conserva:

    Assumo : Inv : 0 ≤ q^2 ≤ √n , x = q^2, y = 2q+1, y+z = n, x>z
    Dimostro : se R è la parte della radice allora R^2 < n < (R+1)^2
    Voglio in sostanza che R sia più piccolo di n, ma che il successivo valore
    sia maggiore di n, notiamo che le variabili che mantegono questo comportamento
    è q, quindi supponiamo:

    R = q?
    x > z
    Sommo y da entrambe le parti
    y+x > y+z  

    dove so y+z = n
    x = q^2
    y = 2q+1

    q^2+2q+1 = y+x > y+z = n
    (q+1)^2 > n

    Quindi q è la viarabile che cercavamo quindi inseriamo q nel return
     */
    public static int intSqrt(int n){     //Pre n>=0
        int q = 0, x = 0, y = 1, z = n-1;
        while(x <= z ){                      // Inv : 0 ≤ q ≤ √n , x = q^2, y = 2q+1, y+z = n,q = q +1;                       //
            // Term: n-q        
            x = x + y;
            y = y + 2;
            z = z - 2;
        }
        return q;                         //Post: valore resituito |sqrt(n)|
    }

    /**
     * 2.Programmazione in Java
     * Scrivi un metodo statico in Java che, data una parola w ed un array di parole sw, restituisca la componente di ws che
     * "approssima meglio" w, nel senso che condivide con w la sottosequenza comune di caratteri più lunga fra tutte le parole
     * di ws. Per esempio, se w= "tuscania" e l' array di parole contiene "bevagna", "saturnia", "pitigliano", "trevi" e 
     * "vitorchiano", allora la soluzione è "saturnia". (Supponi che i metodi statici lcs e llcs siano già disponibili).
     */ 

    public static String bestWord(String w, String[] ws){ //ws.length > 0 
        
        int k=0;  //indice della parola migliore
        int best = llcs( w,ws[k] ); //indice dell' ultima migliore parola
        
        for(int i =0; i< ws.length; i++){
            int curr = llcs( w,ws[i] );
            if( curr > best){
              k=1;
              best = curr;
            };
        }
        return ws[k];
    }
    
    
    public static int llcs(String a, String b){
        return 2;
    }
}
    