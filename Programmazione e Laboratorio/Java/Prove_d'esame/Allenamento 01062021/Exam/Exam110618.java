
/**
 * Aggiungi qui una descrizione della classe Exam110618
 * 
 * @author (il tuo nome) 
 * @version (un numero di versione o una data)
 */
public class Exam110618
{
    /**
     * 3. Memoization
    Applica la tecnica top-down di memoization per realizzare una versione più efficiente del seguente programma in Java:
     */

    public static int q( int[] s ) { // s.length > 0
        int n = s.length;
        int[] t = new int[ n ]; t[0] = s[0];
        for ( int k=1; k<n; k=k+1 ) {
            int i=k-1;
            while ( (i >= 0) && (t[i] > s[k]) ) {
                t[i+1] = t[i]; i = i - 1;
            }
            t[i+1] = s[k];
        }
        return qRec( s, t, n, 0, 0 );
    }

    private static int qRec( int[] s, int[] t, int n, int i, int j ) {
        if ( (i == n) || (j == n) ) {
            return 0;
        } else if ( s[i] == t[j] ) {
            return 1 + qRec( s, t, n, i+1, j+1 );
        } else {
            return Math.max( qRec(s,t,n,i+1,j), qRec(s,t,n,i,j+1) );
        }
    }

    public static int qMem( int[] s ) { // s.length > 0
        int n = s.length;
        int[] t = new int[ n ]; t[0] = s[0];

        int [][] mem = new int[n+1][n+1];

        /*
        int [][][] mem = new int[n+1][n+1][];
        for(int r = 0; r<= n; r++){
        for(int c=0; c<=n ; c++){
        mem[r][c] = null;
        }
        }*/
        for ( int k=1; k<n; k=k+1 ) {
            int i=k-1;
            while ( (i >= 0) && (t[i] > s[k]) ) {
                t[i+1] = t[i]; i = i - 1;
            }
            t[i+1] = s[k];
        }
        return qTD( s, t, n, 0, 0,mem );
    }

    private static int qTD( int[] s, int[] t, int n, int i, int j, int[][] mem ) {
        if ( (i == n) || (j == n) ) {
            mem[i][j] = 0;
        } else if ( s[i] == t[j] ) {
            mem[i][j] = 1 + qTD( s, t, n, i+1, j+1, mem );
        } else {
            mem[i][j] =  Math.max( qTD(s,t,n,i+1,j,mem), qTD(s,t,n,i,j+1,mem) );
        }
        return mem[i][j];
    }

    /**
     * 4. Classi in Java
    Un’istanza della classe ProximityStructure consente di modellare una collezione di misure, rappresentate da valori
    di tipo double, manipolabile attraverso il seguente protocollo:
    public static void Pro (){
        new ProximityStructure() // costruisce una collezione vuota di misure

        s.size() // restituisce il numero di misure contenute nella collezione

        s.add( x ) // aggiunge la misura x alla collezione s

        s.removeClosestTo( x ) // rimuove da s e restituisce la misura più prossima a x  // (la cui distanza da x è più piccola) in s
    }
    
     
    Completa la defnizione della classe ProximityStructure introducendo opportune variabili d’istanza
    (rappresentazione interna) e realizzando il costruttore e i metodi coerentemente con le scelte implementative fatte.�
     */
}
