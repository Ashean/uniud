
/**
 * Aggiungi qui una descrizione della classe Exam18062015
 * 
 * @author (il tuo nome) 
 * @version (un numero di versione o una data)
 */
public class Exam18062015
{
    // variabili d'istanza - sostituisci l'esempio che segue con il tuo
    /**
     * 1. Verifica formale della correttezza
    Dati due interi positivi m, n, il seguente metodo statico restituisce una coppia (array) di interi i, j tali che il valore
    dell’espressione im + jn sia il massimo comun divisore di m, n. Nel programma sono riportate precondizione,
    postcondizione, invariante e funzione di terminazione. Introduci opportune espressioni negli spazi denotati a tratto
    punteggiato in modo tale che i valori assunti dalle variabili soddisfino le relazioni specificate dalle asserzioni.

    public static int[] extGcd( int m, int n ) {                   // Pre: m, n > 0

    int x = m, u = 1, v = 0 ;
    int y = n, i = 0, j = 1 ;
    int q = x / y, r = 0 ;
    while ( ?? ) {                                             // Inv: x, y > 0, x = qy + r, 0 ≤ r < y,
    // MCD(x,y) = MCD(m,n), x = um+vn, y = im+jn
    x = y; y = r;                                              // Term: y
    int s = u - q * i;
    u = i; i = s;
    int t = v - ?? ;
    v = ?? ;
    j = t;
    q = x / y; r = ?? ;
    }
    return new int[] { i, j };                                // Post: MCD(m,n) = im + jn
    }

    DIMOSTRAZIONE DI CORRETTEZZA DEL PROGRAMMA ITERATIVO

    a. L' invariante vale all' inizio

    Inv: x, y > 0,    x = qy + r,    0 ≤ r < y,    MCD(x,y) = MCD(m,n), x = um+vn, y = im+jn

    - Se Pre : m,n > 0, dato che x = m e y = n,  allora    x,y > 0    OK!
    - x = qy + r ->  m = (m/n * n) + ?? -> r = 0
    - Se r = 0, allora x = qy + r OK! && 0 ≤ r < y
    - MCD(x,y) = MCD(m,n) dato che x=m, =n OK!
    - x = um+vn -> m= m + ??*n -> v = 0 OK!
    - y = im+jn -> n = 0*m+??*n -> n = ??*n -> ?? = n/n dove in questo caso ?? = j ,quindi j = n/n ossia 1  

    Inv: m, n > 0,    m = qn + r,    0 ≤ r < n,    MCD(m,n) = MCD(m,n), m = um+vn, n = im+jn
    Inv: m, n > 0,    m = m/n * n + r,    0 ≤ r < n,    MCD(m,n) = MCD(m,n), m = m+vn, n = 0m+jn
    Inv: m, n > 0,    m = m + r,     0 ≤ r < n,    MCD(m,n) = MCD(m,n), 0=vn, n = jn     
    Inv: m, n > 0,    m = m/n * n + r,    0 ≤ r < n,    MCD(m,n) = MCD(m,n), v=0, j=1
    Inv: m, n > 0,    m = m + r,    0 ≤ r < n,    MCD(m,n) = MCD(m,n), v=0, j=1
    Inv: m, n > 0,    r = 0,    0 ≤ r < n,    MCD(m,n) = MCD(m,n), v=0, j=1

        
    b. L'invariante si conserva durante l'iterazione

    Inv() & ('UNKNOWN COND') => Inv'

    Inv: x, y > 0,    x = qy + r,    0 ≤ r < y,    MCD(x,y) = MCD(m,n), x = um+vn, y = im+jn
    Inv: x', y' > 0,    x' = q'y' + r',    0 ≤ r' < y',    MCD(x',y') = MCD(m,n), x' = u'm+v'n, y' = i'm+j'n

    Inv: y, y' > 0,    y = q'y' + r',    0 ≤ r' < y',    MCD(x',y') = MCD(m,n), y = u'm+v'n, y' = i'm+j'n

    Inv: y, r > 0,    y = q'r + r',    0 ≤ r' < r,    MCD(y,r) = MCD(m,n), y = u'm+v'n, r = i'm+j

    Inv: y, r > 0,    y = q'r + r',    0 ≤ r' < r,  s = u - q*i,   MCD(y,r) = MCD(m,n), y = u'm+v'n, r = i'm+j

    Inv: y, r > 0,    y = q'r + r',    0 ≤ r' < r,  s = u - q*i,   MCD(y,r) = MCD(m,n), y = im+v'n, r = i'm+j

    Inv: y, r > 0,    y = q'r + r',    0 ≤ r' < r,  s = u - q*i,   MCD(y,r) = MCD(m,n), y = im+v'n, r = sm+j

    Inv: y, r > 0,    y = q'r + r',    0 ≤ r' < r,  s = u - q*i, t = v -??, j' = t   MCD(y,r) = MCD(m,n), y = im+v'n, r = sm+j'n

    Inv: y, r > 0,    y = x'/y'r + r',    0 ≤ r' < r,  s = u - q*0, t = v -??, j' = t   MCD(y,r) = MCD(m,n), y = 0m+v'n, r = sm+j'n

    Inv: y, r > 0,    y = x'/y'r + r',    0 ≤ r' < r,  s = u , t = v -??, j' = t   MCD(y,r) = MCD(m,n), y = v'n, r = um+j'n

    Inv: y, r > 0,    y = x'/y'r + r',    0 ≤ r' < r,  s = u , t = v -??, j' = t   MCD(y,r) = MCD(m,n), y = v'n, r = m+ tn

    Inv: y, r > 0,    y = x'/y'r + r',    0 ≤ r' < r,  s = u , t = v -??, j' = t   MCD(y,r) = MCD(m,n), v' = n/n = j, r = um+j'n

    Inv: y, r > 0,    y = x'/y'r + r',    0 ≤ r' < r,  s = u , t = v -??, j' = t   MCD(y,r) = MCD(m,n), v' = n/n = j, r-m/n = +t

    Inv: y, r > 0,    y = x'/y'r + r',    0 ≤ r' < r,  s = u , r-n/m = v -??, j' = r- m/n   MCD(y,r) = MCD(m,n), v' = n/n = j, r-m/n = +t

    DA TERMINARE
     */
    /**2. Classi in Java
     * 
    La classe HiddenWord realizza un modello del gioco della “parola nascosta” in cui il conduttore del gioco sceglie una
    parola (nel caso di un programma la scelta può essere effettuata casualmente a partire da un vocabolario), dicendo di
    quante lettere si compone, mentre gli altri partecipanti cercano di indovinarla proponendo a turno delle ipotesi. Ad ogni
    proposta il conduttore risponde mostrando una stringa delle corrispondenze, dove le lettere che occupano esattamente la
    stessa posizione nella parola nascosta e nell’ultima ipotesi formulata sono “scoperte”, mentre tutte le altre sono
    sostituite da asterischi (incluse eventualmente quelle per cui non si può stabilire una corrispondenza perché la parola
    ipotizzata è più corta della parola nascosta). Naturalmente vince chi alla fine riesce a riconoscere la parola nascosta. In
    sintesi, per la classe HiddenWord è definito il seguente protocollo:
    
    HiddenWord( String hw ) costruttore, a cui la parola nascosta è passata come argomento
    
    int length() metodo che restituisce lunghezza della parola nascosta
    
    String guess( String gw ) metodo che, data un’ipotesi, restituisce la stringa delle corrispondenze
    
    Per esempio, dopo aver creato l’istanza hw = new HiddenWord("pluto"), valutando nell’ordine le espressioni
    hw.length(), hw.guess("pippo"), hw.guess("paperino"), hw.guess("terra"), hw.guess("blu"),
    hw.guess("prato") si ottiene rispettivamente 5, "p***o", "p****", "*****", "*lu**" e "p**to".
    Realizza in Java la classe HiddenWord rispettando le specifiche illustrate sopra.�
    
    
    VEDI CLASSE HIDDEN WORD
    */
    
    
}
