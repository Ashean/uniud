;; The first three lines of this file were inserted by DrRacket. They record metadata
;; about the language level of this file in a form that our tools can easily process.
#reader(lib "htdp-intermediate-lambda-reader.ss" "lang")((modname Esame_20190119) (read-case-sensitive #t) (teachpacks ((lib "drawings.ss.txt" "installed-teachpacks"))) (htdp-settings #(#t constructor repeating-decimal #f #t none #f ((lib "drawings.ss.txt" "installed-teachpacks")) #f)))
;  Corso di Programmazione
;  I Prova di accertamento del 21 Gennaio 2019 / A cognome e nome
;  Riporta in modo chiaro negli appositi spazi le soluzioni degli esercizi, oppure precise indicazioni se alcune soluzioni si trovano in un
;  foglio separato. Scrivi inoltre il tuo nome nelle intestazioni e su ciascun ulteriore foglio che intendi consegnare

;1. Programmi in Scheme

(define f ; val: intero
  (lambda (x y) ; x ≥ 0, y > 0 interi
    (if (< x y)
        1
        (+ (f (- x 1) y) (f (- x y) y))
        )
    )
  )



;2. Programmazione in Scheme
;  Una stringa s è una palindrome se procedendo da sinistra a destra oppure da destra a sinistra si legge la stessa sequenza
;  di caratteri, come nel caso di "esose". Scrivi un programma in Scheme per realizzare la procedura a valori interi
;  palindrome-lev per determinare il “livello di palindromicità” di una data stringa, cioè il numero di coppie di
;  caratteri uguali in posizioni simmetriche rispetto al centro della stringa (quindi alla stessa distanza rispetto agli estremi),
;  contando anche l’eventuale carattere autosimmetrico che si trovi esattamente al centro della stringa. Esempi:

(define palindrome-lev
  (lambda (s)
    (if (string=? s "")
        0
        (let ((len (string-length s)))
          (if(= len 1)
             1
             (if (char=?(string-ref s 0) (string-ref s (- len 1)))
                 (+ 1 (palindrome-lev (substring  s 1 (- len 1))))
                 (palindrome-lev (substring  s 1 (- len 1)))
                 )
             )
          )
        )
    )
  )


(palindrome-lev "")  
(palindrome-lev "a")  
(palindrome-lev "nono")  
(palindrome-lev "esose")
(palindrome-lev "erodere")
(palindrome-lev "ilredevevederli")


;  Il programma impostato nel riquadro applica la logica risolutiva del problema della sottosequenza comune più lunga
;  (LCS) per confrontare una stringa di riferimento s con una diversa versione t, dove si assume che sia s che t siano
;  costituite esclusivamente da lettere maiuscole o minuscole, non da altri tipi di carattere. In particolare, la procedura
;  xlcs restituisce una stringa così composta: le lettere di s che trovano corrispondenza in t sono sostituite da un
;  asterisco *; quelle da “cancellare” nella sottosequenza comue più lunga sono rimpiazzate da una barra inclinata /
;  inoltre, le lettere di t da “aggiungere” (cioè senza corrispondenza) rispetto alla stringa di riferimento s vengono incluse
;  nelle posizioni corrispondenti. Esempi:
;  Completa il programma inserendo espressioni appropriate negli spazi indicati.

(define xlcs ; val: stringa
  (lambda (s t) ; s, t: stringhe
    (cond ((string=? s "") t)
          ((string=? t "")
           (string-append "/" (xlcs (substring s 1) t)))
          ((char=? (string-ref s 0) (string-ref t 0))
           (string-append "*" (xlcs (substring s 1) (substring t 1))))
          (else
           (better
            (string-append "/" (xlcs (substring s 1) t))
            (string-append (substring t 0 1) (xlcs s (substring t 1)))
                   
                   ))
          )))


(define better
  (lambda (u v)
    (if (< (stars u) (stars v))
        v
        u
        ))
  )


(define stars
  (lambda (q)
    (if (string=? q "")
        0
        (let ((n (stars (substring q 1))))
          (if (char=? (string-ref q 0) #\*) (+ n 1) n)
          ))
    )
  )

(xlcs "" "")
(xlcs "" "ma")
(xlcs "ma" "") 
(xlcs "ma" "ma") 
(xlcs "arto" "atrio")
 (xlcs "atrio" "arto")
(xlcs "flora" "lira")
(xlcs "cincia" "piani")


;5. Procedure con argomenti procedurali
;   Il programma impostato nel riquadro risolve il problema dei percorsi di Manhattan e restituisce una lista di interi,
;   contenente tanti elementi quanti sono i percorsi diversi. Più specicamente, ciascun percorso è rappresentato nella lista
;   dal numero di cambi di direzione — o svolte — che lo caratterizzano. Nel caso di una “mappa” reticolare 2 x 2, per
;   esempio, sono possibili 6 percorsi diversi, che a due a due richiedono rispettivamente 1, 2 o 3 svolte; se punto di
;   partenza e destinazione si trovano sulla stessa “strada”, allora c’è un unico percorso senza svolte; se un lato del
;   rettangolo ha lunghezza unitaria, sono possibili due percorsi con una sola svolta lungo il perimetro e altri percorsi con
;   due svolte con attraversamenti all’interno del rettangolo:
;   (mh 2 2) → (1 3 2 2 3 1) (mh 0 5) → (0) (mh 4 1) → (1 2 2 2 1)
;Completa il programma inserendo espressioni appropriate negli spazi indicati.

(define mh ; val: lista di interi
  (lambda (i j) ; i, j: interi non negativi
    (if (or (= i 0) (= j 0))
        (list 0)
        (append (mx true (- i 1) j) (mx false i (- j 1)))
 )
    )
  )

(define mx ; down: booleano = true se passo precedente in giù / = false se a destra
  (lambda (down i j)
    (cond ((and (= i 0) (= j 0))
           (list 0))
          ((= i 0)
           (list (if down 1 0)))
          ((= j 0)
           (list (if down 0 1)))
          (down
           (append (mx true (- i 1) j)
                   (map (lambda (x) (+ x 1))(mx false i (- j 1)))
                   ))
 (else
 (append
         (map (lambda (x) (+ 1 x)) (mx true (- i 1) j ))
          (mx false i (- j 1))

 ))
 )))
(mh 2 2)
(mh 0 5)
(mh 4 1) 


