int margin = 20;
int rows = 17;
int cols = 17;
float dx, dy, side;

void setup() {
  size(700, 700);
  background(250, 245, 245);
  noFill();
  side = (width - margin * 2) / cols;
  for (int j = 0; j < cols; j++) { 
    for (int i = 0; i < rows; i++) { 
      drawQuad(margin + i * side, margin + j * side, side);
    }
  }
}

void drawQuad(float x, float y, float side) {
  int nsquare = (int) random(5, 10);
  float x1, y1, x2, y2, x3, y3, x4, y4;
  int r = 2;
  int c = 100;
  int red = 101, green = 87, blue = 63;
  float l = side / (2 * nsquare);
  pushMatrix();
  translate(x, y);
  for (int i = 0; i < nsquare; i++) {
    x1 = 0 + random(r) + l*i;
    y1 = 0 + random(r) + l*i;
    x2 = side + random(r) - l*i;
    y2 = 0 + random(r) + l*i;
    x3 = side + random(r) - l*i;
    y3 = side + random(r) - l*i;
    x4 = 0 + random(r) + l*i;
    y4 = side + random(r) - l*i;
    strokeWeight(1 * random(3));
    stroke(red + random(-c, c), green  + random(-c, c), blue  + random(-c, c));
    quad(x1, y1, x2, y2, x3, y3, x4, y4);
  }
  popMatrix();
}
