/* 
 Generative Art Recoded
 Schotter (Gravel)
 Georg Nees, 1968
 hex6c, 2020
*/

int margin = 60;
int n = 12;
int m = 21;
float dx, dy, angle, side, distorsion;

void setup() {
  size(496, 714);
  background(255);
  distorsion = 1.5;
  side = (width - margin * 2) / n;
  for (int j = 0; j < m; j++) { 
    for (int i = 0; i < n; i++) { 
      float r = j * distorsion;
      dx = random(r);
      dy = random(r);
      angle = random(-r, r);
      tile(margin + i * side, margin / 3 + j * side, dx, dy, side, angle);
    }
  }
}

void draw() {
}

void tile(float x, float y, float dx, float dy, float side, float angle) {
  pushMatrix();
  translate(x + dx, y + dy);
  rotate(radians(angle));
  rect(0, 0, side, side);
  popMatrix();
}
