int nrows = 10;
int ncols = 8;
float[][] Mx = new float[nrows][ncols];
float[][] My = new float[nrows][ncols];
int margin = 20;
int noise = 20;

void setup() {
  size(600, 600);
  background(241, 237, 233);
  ellipseMode(RADIUS);
  noFill();
  float sidex = (width - 2 * margin) / (ncols - 1);
  float sidey = (height - 2 * margin) / (nrows - 1);
  for (int j = 0; j < ncols; j++) {
    for (int i = 0; i < nrows; i++) {
      Mx[i][j] = margin + sidex * j + random(-noise, noise);
      My[i][j] = margin + sidey * i + random(-noise, noise);
    }
  }
  strokeWeight(1);
  for (int j = 0; j < ncols - 1; j++) {
    for (int i = 0; i < nrows; i++) {
      line(Mx[i][j], My[i][j], Mx[i][j+1], My[i][j+1]);
    }
  }

  for (int j = 0; j < ncols - 1; j++) {
    for (int i = 0; i < nrows - 1; i++) {
      if (random(1) < 0.5) drawSegments(i, j);
    }
  }

  for (int i = 1; i <= 5; i++) {
    float radius = random(3, 40);
    float x = random(radius, width - radius);
    float y = random(radius, height - radius);
    ellipse(x, y, radius, radius);
  }
}

void drawSegments(int i, int j) {
  float x1 = Mx[i][j], y1 = My[i][j], x2 = Mx[i][j+1], y2 = My[i][j+1];
  float x3 = Mx[i+1][j], y3 = My[i+1][j], x4 = Mx[i+1][j+1], y4 = My[i+1][j+1];
  int n = (int) random(5, 15);
  boolean dice = (random(1) < 0.15);
  strokeWeight(1);
  for (int k = 0; k < n; k++) {
    float m1 = (y2 - y1) / (x2 - x1);
    float x = random(x1, x2);
    float y = m1 * (x - x1) + y1;
    float m2 = (y4 - y3) / (x4 - x3);
    float w;
    if (dice) 
      w = x; 
    else 
    w = random(x3, x4);
    float z = m2 * (w - x3) + y3;
    line(x, y, w, z);
  }
}
