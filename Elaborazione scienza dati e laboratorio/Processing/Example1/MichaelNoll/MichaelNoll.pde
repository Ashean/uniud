/* 
 Generative Art Recoded
 Computer Composition With Lines 
 Michael Noll, 1964
 hex6c, 2020
 */

float inf, sup, radius;
int cardinality = 400;

void setup() {
  size(600, 600);
  background(255);
  radius = width / 2;
  sup = radius / 8;
  inf = 5;
  strokeWeight(6);
  strokeCap(SQUARE);
  int count = 0;
  while (count < cardinality) {
    float x = random(width);
    float y = random(height);
    if (dist(x, y, width/2, height/2) <= (radius - sup)) {
      drawLine(x, y);
      count++;
    }
  }
}

void draw() {
}

void drawLine(float x, float y) {
  if (random(1) < 0.5) {
    if (random(1) < 0.5) {
      line(x, y, x + random(inf, sup), y);
    } else {
      line(x, y, x - random(inf, sup), y);
    }
  } else {
    if (random(1) < 0.5) {
      line(x, y, x, y + random(inf, sup));
    } else {
      line(x, y, x, y - random(inf, sup));
    }
  }
}
